import java.io.File;

import mmlib4j.debug.DebugTime;
import mmlib4j.features.ExtractFeatures;
import mmlib4j.features.Feature;
import mmlib4j.features.MeanVolumeFeatureR;
import mmlib4j.features.MserFeature;
import mmlib4j.features.NormalFeature;
import mmlib4j.files.DataFile;
import mmlib4j.files.DataLoader;
import mmlib4j.images.ColorImage;
import mmlib4j.primitives.Primitives;
import mmlib4j.representation.tree.attribute.Attribute;
import mmlib4j.representation.tree.componentTree.ConnectedFilteringByComponentTree;
import mmlib4j.targets.BoundingBoxMatching;
import mmlib4j.targets.Target;
import mmlib4j.utils.AdjacencyRelation;
import mmlib4j.utils.ImageBuilder;
import mmlib4j.utils.Utils;

public class Test {
	
	// Define features identifiers
	public static final int MEAN_VOLUME_R = 100;
	
	public static void main(String args[]) {
		
		// Mmlib degub off
		Utils.debug = false;
		
		// Ml debug
		DebugTime.newInstance();
		
		// Loader files
		String inputPath   = "../Ismm/dataset/Tray/";	
		String outputPath  = "/Users/charles/Desktop/results/";
		
		String datasets [] = {"Ara2012", 
							  "Ara2013-Canon", 
							  "Ara2013-RPi"};
		
		DataLoader loader = new DataLoader(inputPath, outputPath, datasets);				
		
		// Create features vector from attributes
		Feature features[] = {
				
			new NormalFeature("RATIO_WIDTH_HEIGTH", Attribute.RATIO_WIDTH_HEIGHT),
			new MserFeature(10, "MSER",             Attribute.MSER),
			new MeanVolumeFeatureR("MEAN_VOLUME_R", MEAN_VOLUME_R)
				
		};			
	
		// Extracting
		for(DataFile dataFile : loader.dir().getListOfFiles()) {
			
			File imgFile = dataFile.filter("_rgb.png");
			File ground  = dataFile.filter(".csv");
			
			Target target = new BoundingBoxMatching(ground);
			
			ColorImage imgRGB = ImageBuilder.openRGBImage(imgFile);
			
			ConnectedFilteringByComponentTree tree = new ConnectedFilteringByComponentTree(imgRGB.getGreen(), AdjacencyRelation.getAdjacency8(), true);
			
			Primitives primitives = new Primitives(tree);
			
			ExtractFeatures extract = new ExtractFeatures(tree, imgRGB, primitives.getNodesNr(), primitives.getNumValidNodes(), features, target);
			
			extract.load().find(2).save(dataFile.output(), dataFile.prefix("_rgb.png"));					
			
			extract.load().find(2).Y().head(200, 1);
						
			break;
			
		}
		
	}

}
