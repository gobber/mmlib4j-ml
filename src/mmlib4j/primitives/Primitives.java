package mmlib4j.primitives;

import java.util.List;
import mmlib4j.representation.tree.componentTree.ComponentTree;
import mmlib4j.representation.tree.componentTree.NodeCT;
import mmlib4j.representation.tree.pruningStrategy.MappingStrategyOfPruning;

public class Primitives {
	
	private boolean nodesNr[];
	
	private ComponentTree tree;
	
	private int numValidNodes;

	private List<MappingStrategyOfPruning> primitives;
	
	public Primitives(ComponentTree tree) {
		
		this.tree = tree;
		
		nodesNr = new boolean[tree.getNumNode()];
		
		setNumValidNodes(tree.getNumNode());
		
		for(NodeCT node : tree.getListNodes()) {
			
			nodesNr[node.getId()] = true;
			
		}
		
	}
	
	public Primitives(ComponentTree tree, List<MappingStrategyOfPruning> primitives) {
		
		this.tree = tree;
		
		this.primitives = primitives;
		
	}

	public Primitives intersect() {
		
		setNumValidNodes(0);
		
		for(NodeCT node: tree.getListNodes()) {			
			
			for(MappingStrategyOfPruning primitive : primitives) {
				
				boolean selectednodes[] = primitive.getMappingSelectedNodes();
				
				// Verificar
				if(nodesNr[node.getId()] = nodesNr[node.getId()] & selectednodes[node.getId()]) {
					numValidNodes++;
				}
				
			}
			
		}
		
		return this;
		
	}
	
	public boolean[] getNodesNr() {
		
		return nodesNr;
		
	}

	public void setNodesNr(boolean nodesNr[]) {
		
		this.nodesNr = nodesNr;
		
	}

	public int getNumValidNodes() {
		return numValidNodes;
	}

	public void setNumValidNodes(int numValidNodes) {
		this.numValidNodes = numValidNodes;
	}
	
}
