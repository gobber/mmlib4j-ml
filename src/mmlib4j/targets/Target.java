package mmlib4j.targets;

import mmlib4j.representation.tree.componentTree.NodeCT;

public interface Target {
		
	double match(NodeCT node);

}
