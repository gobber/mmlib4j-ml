package mmlib4j.models.postprocessing;

import mmlib4j.models.datastruct.Matrix;

public class PostProcessingIdentity<N> implements PostProcessing<N>{
	@Override
	public Matrix<N> execute(Matrix<N> output) {
		return output;
	}
}
