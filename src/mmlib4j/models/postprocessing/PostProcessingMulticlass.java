package mmlib4j.models.postprocessing;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.postprocessing.PostProcessing;

public class PostProcessingMulticlass<N> implements PostProcessing<N> {
	@Override
	public Matrix<N> execute(Matrix<N> output) {
		return output.argmax(1);
	}		
}
