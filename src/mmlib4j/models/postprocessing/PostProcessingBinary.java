package mmlib4j.models.postprocessing;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.postprocessing.PostProcessing;

public class PostProcessingBinary<N> implements PostProcessing<N> {	
	@Override
	public Matrix<N> execute(Matrix<N> output) {
		return output.biggertheni(0.5);
	}
}
