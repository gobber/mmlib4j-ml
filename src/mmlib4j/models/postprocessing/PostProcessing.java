package mmlib4j.models.postprocessing;

import mmlib4j.models.datastruct.Matrix;

public interface PostProcessing<N> {

	public Matrix<N> execute(Matrix<N> output);
	
}
