package mmlib4j.models.neural_network;

public class NetworkFloat32 implements Network<float[]> {

	private String hidden_activation;
    private String output_activation;
    private float[][] weights;        
    private float[][] bias;
    private int[] numRows;
    private int[] numColumns;
	
	@Override
	public String hidden_activation() {
		return hidden_activation;
	}

	@Override
	public String output_activation() {
		return output_activation;
	}

	@Override
	public float[][] weights() {
		return weights;
	}

	@Override
	public float[][] bias() {
		return bias;
	}

	@Override
	public int biasLength(int row) {
		return bias[row].length;
	}

	@Override
	public int numRows(int row) {		
		return numRows[row];
	}

	@Override
	public int numColumns(int column) {
		return numColumns[column];
	}

}
