package mmlib4j.models.neural_network;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.Models;
import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.neural_network.Layer;
import mmlib4j.models.neural_network.Network;
import mmlib4j.models.neural_network.NetworkFloat32;
import mmlib4j.models.neural_network.NetworkFloat64;
import mmlib4j.models.postprocessing.PostProcessing;
import mmlib4j.models.postprocessing.PostProcessingBinary;
import mmlib4j.models.postprocessing.PostProcessingIdentity;
import mmlib4j.models.postprocessing.PostProcessingMulticlass;
import mmlib4j.models.preprocessing.Scaler;
import mmlib4j.models.transfer.Transfer;

public class MLP<N> extends Models<N> {
	
	private PostProcessing<N> post;
	private ArrayList<Layer<N>> layers = new ArrayList<Layer<N>>();
	private Class<N> modelType;
	private Scaler<N> scaler;
	private Network<N> net;
	
	MLP() {}
	
	public MLP(Class<N> modelType, String modelData) {
		this.modelType = modelType;
		load(modelData);
	}
	
	public MLP(Class<N> modelType, JsonObject jsonObject){
		this.modelType = modelType;
		load(jsonObject);
	}
	
	public MLP<N> load(String modelData){
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();        
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();        
        load(jsonObject);                      
        return this;
	}
		
	@SuppressWarnings("unchecked")
	public MLP<N> load(JsonObject jsonObject){
		switch (modelType.getCanonicalName()) {
		case "double[]":					
			net = (Network<N>) new Gson().fromJson(jsonObject, NetworkFloat64.class);										
			break;
		case "float[]":
			net = (Network<N>) new Gson().fromJson(jsonObject, NetworkFloat32.class);						
			break;
		}		   		
		for(int l = 0 ; l < net.weights().length-1 ; l++) {
			layers.add(new Layer<N>(new Transfer<>(modelType, net.hidden_activation()),
									new Matrix<>(modelType, net.numRows(l), net.numColumns(l), net.weights()[l], false),
									new Matrix<>(modelType, 1, net.biasLength(l), net.bias()[l], false)));
		}		
		layers.add(new Layer<N>(new Transfer<>(modelType, net.output_activation()),
				  new Matrix<>(modelType, net.numRows(net.weights().length-1), net.numColumns(net.weights().length-1), net.weights()[net.weights().length-1], false),
				  new Matrix<>(modelType, 1, net.biasLength(net.weights().length-1), net.bias()[net.weights().length-1], false)));    	    					
		String type = jsonObject.get("type").getAsString();        
    	if(type.contains("MLPBinaryClassifier")) {
    		post = new PostProcessingBinary<>();
        }else if(type.contains("MLPMultiClassifier")) {
        	post = new PostProcessingMulticlass<>();
        } else if(type.contains("MLPRegressor")){
        	post = new PostProcessingIdentity<>();
        } else {
        	throw new IllegalArgumentException("Invalid model type");
        }       		
    	this.scaler = new Scaler<>(modelType, jsonObject);
        return this;
	}
	
	@Override
	public Matrix<N> predict(Matrix<N> x) {		
		Matrix<N> a = scaler.normalize(x);
		Matrix<N> z;		
		for(Layer<N> layer : layers) {
			z = a.dot(layer.getWeigths()).plusi(layer.getBias());		
			a = layer.activate(z);		
		}		
		return post.execute(a);		
	}	
	
	public static void main(String args[]) {
		
		// Parameters:
        String modelData = "/Users/charles/Desktop/data.json";
        
        double[][] features = {{5.1, 3.5, 1.4, 0.2, 5.1, 3.5, 1.4, 0.2}, 
        					   {5.9, 3. , 5.1, 1.8, 5.9, 3. , 5.1, 1.8},
   							   {5.5, 2.4, 3.8, 1.1, 5.5, 2.4, 3.8, 1.1},
   							   {6.3, 3.3, 6. , 2.5, 6.3, 3.3, 6. , 2.5}}; 
        
        Matrix<double[]> matrix = new Matrix<>(features);		
        
        MLP<double[]> mlp = new MLP<>(double[].class, modelData);
        mlp.predict(matrix).print();
        
        
	}
	
}


