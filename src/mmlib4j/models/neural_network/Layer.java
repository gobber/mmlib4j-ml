package mmlib4j.models.neural_network;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.transfer.Transfer;

public class Layer<N> {
	
	private Transfer<N> transfer;
	private Matrix<N> weigths;
	private Matrix<N> bias;
	
	public Layer(Transfer<N> transfer, Matrix<N> weigths, Matrix<N> bias) {
		this.transfer = transfer;
		this.setWeigths(weigths);
		this.setBias(bias);
	}
	
	public Layer(Transfer<N> transfer) {
		this.transfer = transfer;
	}
	
	public Matrix<N> activate(Matrix<N> matrix) {
		return transfer.activate(matrix);	
	}
	
	public Transfer<N> getTransfer() {
		return transfer;
	}
	
	public void setTransfer(Transfer<N> transfer) {
		this.transfer = transfer;
	}

	public Matrix<N> getWeigths() {
		return weigths;
	}

	public void setWeigths(Matrix<N> weigths) {
		this.weigths = weigths;
	}

	public Matrix<N> getBias() {
		return bias;
	}

	public void setBias(Matrix<N> bias) {
		this.bias = bias;
	}

}
