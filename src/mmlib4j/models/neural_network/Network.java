package mmlib4j.models.neural_network;

public interface Network<N> {
	public String hidden_activation();
	public String output_activation();
    public N[] weights();        
    public N[] bias();
    public int biasLength(int row);
    public int numRows(int row);
    public int numColumns(int column);	
}
