package mmlib4j.models.linear;

public class ClassifierFloat32 implements Classifier<float[]>{

	float coefficients[];
	float intercept[];
	int numRows;
	int numColumns;
	
	@Override
	public float[] coefficients() {
		return coefficients;
	}

	@Override
	public float[] intercept() {
		return intercept;
	}

	@Override
	public int numRows() {
		return numRows;
	}

	@Override
	public int numColumns() {
		return numColumns;
	}

	@Override
	public int interceptsLength() {
		return intercept.length;
	}

}
