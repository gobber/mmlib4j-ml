package mmlib4j.models.linear;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.Models;
import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.postprocessing.PostProcessing;
import mmlib4j.models.postprocessing.PostProcessingBinary;
import mmlib4j.models.postprocessing.PostProcessingMulticlass;
import mmlib4j.models.preprocessing.Scaler;
import mmlib4j.models.transfer.Logistic;
import mmlib4j.models.transfer.Softmax;
import mmlib4j.models.transfer.Transfer;

public class LogisticRegression<N> extends Models<N>{
		
	private PostProcessing<N> post;
	private Classifier<N> classifier;		
	private Matrix<N> coefficients;	
	private Matrix<N> intercept;
	private Class<N> modelType;
	private Transfer<N> transfer;
	private Scaler<N> scaler;
	
	LogisticRegression() {}
	
	public LogisticRegression(Class<N> modelType, String modelData) {
		this.modelType = modelType;
		load(modelData);
	}
	
	public LogisticRegression(Class<N> modelType, JsonObject jsonObject){
		this.modelType = modelType;
		load(jsonObject);
	}
	
	public LogisticRegression<N> load(String modelData){
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();        
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();        
        load(jsonObject);                      
        return this;
	}
	
	@SuppressWarnings("unchecked")	
	public LogisticRegression<N> load(JsonObject jsonObject){
		
		switch (modelType.getCanonicalName()) {
		case "double[]":					
			classifier = (Classifier<N>) new Gson().fromJson(jsonObject, ClassifierFloat64.class);										
			break;
		case "float[]":
			classifier = (Classifier<N>) new Gson().fromJson(jsonObject, ClassifierFloat32.class);						
			break;
		}		            
        this.coefficients = new Matrix<>(modelType, classifier.numRows(), classifier.numColumns(), classifier.coefficients(), false);        
        this.intercept = new Matrix<>(modelType, 1, classifier.interceptsLength(), classifier.intercept(), false);        
        this.scaler = new Scaler<>(modelType, jsonObject);
        
		String type = jsonObject.get("type").getAsString();
		
    	if(type.contains("MultinomialLogistic")) {
    		post = new PostProcessingMulticlass<>();
    		transfer = new Softmax<>();
        }else if(type.contains("BinaryLogistic")) {
        	post = new PostProcessingBinary<>();
        	transfer = new Logistic<>();
        } else if(type.contains("MulticlassLogistic")){
        	post = new PostProcessingMulticlass<>();
        	transfer = new Logistic<>();
        } else {
        	throw new IllegalArgumentException("Invalid model type");
        } 
    	
    	return this;
	}
	
	public Matrix<N> probability(Matrix<N> x) {
		return transfer.activate(scaler.normalize(x).dotT(coefficients).plusi(intercept));
	}
	
	@Override
	public Matrix<N> predict(Matrix<N> x) {				
		return post.execute(transfer.activate(scaler.normalize(x).dotT(coefficients).plusi(intercept)));		
	}	
	
	public static void main(String args[]) {
		
		// Parameters:
        String modelData = "/Users/charles/Desktop/data.json";
        
        double[][] features = {{5.1f, 3.5f, 1.4f, 0.2f, 5.1f, 3.5f, 1.4f, 0.2f}, 
        					   {5.9f, 3.f , 5.1f, 1.8f, 5.9f, 3.f , 5.1f, 1.8f},
   							   {5.5f, 2.4f, 3.8f, 1.1f, 5.5f, 2.4f, 3.8f, 1.1f},
   							   {6.3f, 3.3f, 6.f , 2.5f, 6.3f, 3.3f, 6.f , 2.5f}}; 
        
        Matrix<double[]> matrix = new Matrix<>(features);		
        
        LogisticRegression<double[]> logisticRegression = new LogisticRegression<>(double[].class, modelData);
        logisticRegression.probability(matrix).print();
                
	}

}
