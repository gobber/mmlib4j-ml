package mmlib4j.models.linear;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.Models;
import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.preprocessing.Scaler;

public class LinearRegression<N> extends Models<N>{		 
	
	private Class<N> modelType;
	private Matrix<N> coefficients;
	private Matrix<N> intercept;
	private Regression<N> regression;	
	private Scaler<N> scaler;
		
	public LinearRegression(Regression<N> regression) {
		this.coefficients = new Matrix<>(regression.type(), 1, regression.length(), regression.coefficients(), false);
		this.intercept = new Matrix<>(regression.type(), 1, 1, regression.intercept(), false);	
	}
	
	public LinearRegression(Class<N> modelType, JsonObject jsonObject) {
		this.modelType = modelType;
		load(jsonObject);
	}
	
	public LinearRegression(Class<N> modelType, String modelData) {
		this.modelType = modelType;
		load(modelData);
	}
	
	public Matrix<N> predict(Matrix<N> x){
		return scaler.normalize(x).dotT(coefficients).plusi(intercept);
	}

	public LinearRegression<N> load(String modelData){
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();        
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();        
        load(jsonObject);                       
        return this;
	}
	
	@SuppressWarnings("unchecked")
	public LinearRegression<N> load(JsonObject jsonObject) {	
		switch (modelType.getCanonicalName()) {
			case "double[]":					
				regression = (Regression<N>) new Gson().fromJson(jsonObject, RegressionFloat64.class);										
				break;
			case "float[]":
				regression = (Regression<N>) new Gson().fromJson(jsonObject, RegressionFloat32.class);						
				break;
		}				
		this.coefficients = new Matrix<>(modelType, 1, regression.length(), regression.coefficients(), false);
		this.intercept = new Matrix<>(modelType, 1, 1, regression.intercept(), false);
		this.scaler = new Scaler<>(modelType, jsonObject);
		return this;
	}
	
}
