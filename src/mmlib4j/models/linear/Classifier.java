package mmlib4j.models.linear;

public interface Classifier<N> {
	N coefficients();
	N intercept();
	int numRows();
	int numColumns();
	int interceptsLength();
}
