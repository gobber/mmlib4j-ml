package mmlib4j.models.linear;

public class ClassifierFloat64 implements Classifier<double[]>{

	double coefficients[];
	double intercept[];
	int numRows;
	int numColumns;
	
	@Override
	public double[] coefficients() {
		return coefficients;
	}

	@Override
	public double[] intercept() {
		return intercept;
	}

	@Override
	public int numRows() {
		return numRows;
	}

	@Override
	public int numColumns() {
		return numColumns;
	}

	@Override
	public int interceptsLength() {
		return intercept.length;
	}

}
