package mmlib4j.models.linear;

public class RegressionFloat64 implements Regression<double[]> {
	double coefficients[];
	double intercept[];
	@Override
	public double[] coefficients() {
		return coefficients;
	}
	@Override
	public double[] intercept() {
		return intercept;
	}
	@Override
	public int length() {
		return coefficients.length;
	}
	@Override
	public Class<double[]> type() {
		return double[].class;
	}
	
}