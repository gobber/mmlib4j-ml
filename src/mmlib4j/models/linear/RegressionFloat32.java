package mmlib4j.models.linear;

public class RegressionFloat32 implements Regression<float[]>{
	float coefficients[];
	float intercept[];
	@Override
	public float[] coefficients() {
		return coefficients;
	}
	@Override
	public float[] intercept() {
		return intercept;
	}
	@Override
	public int length() {
		return coefficients.length;
	}
	@Override
	public Class<float[]> type() {
		return float[].class;
	}
	
}
