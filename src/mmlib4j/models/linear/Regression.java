package mmlib4j.models.linear;

public interface Regression<N>{	
	public N coefficients();
	public N intercept();
	public Class<N> type();
	public int length();
}
