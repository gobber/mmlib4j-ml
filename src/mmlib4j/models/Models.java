package mmlib4j.models;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidParameterException;
import java.util.Scanner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.linear.LinearRegression;
import mmlib4j.models.neural_network.MLP;
import mmlib4j.models.svm.LinearSVC;
import mmlib4j.models.svm.SVC;

public class Models<N> {
	
	private Models<N> model;
	
	public Models() {}
	
	public Models (Class<N> modelType, JsonObject jsonObject) {
		load(modelType, jsonObject);			
	}

	public Models (Class<N> modelType, String modelData) {
		load(modelType, modelData);			
	}

	public Models<N> load(Class<N> modelType, JsonObject jsonObject){
		String type = jsonObject.get("type").getAsString();    			
		if(type.contains("LinearSVC")) {
			this.model = new LinearSVC<>(modelType, jsonObject);
		} else if(type.contains("SVC")) {
			this.model = new SVC<>(modelType, jsonObject);
		} else if(type.contains("MLP")) {			
			this.model = new MLP<>(modelType, jsonObject);
		} else if(type.contains("LinearRegression")) {
			this.model = new LinearRegression<>(modelType, jsonObject);			
		} else if(type.contains("Logistic")) {
			this.model = new LinearRegression<>(modelType, jsonObject);		
		} else {
			throw new InvalidParameterException("Model is not supported");
		}				
		return this;
	}
	
	public Models<N> load(Class<N> arrayType, String modelData){
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
        String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();
        return load(arrayType, jsonObject);
	}	
	
	public Matrix<N> predict(Matrix<N> x){
		return model.predict(x);
	}
	
	public static void main(String args[]) {
		
		// Parameters:
        String modelData = "/Users/charles/Desktop/data.json";
        
        double[][] features = {{5.1f, 3.5f, 1.4f, 0.2f, 5.1f, 3.5f, 1.4f, 0.2f}, 
        					   {5.9f, 3.f , 5.1f, 1.8f, 5.9f, 3.f , 5.1f, 1.8f},
   							   {5.5f, 2.4f, 3.8f, 1.1f, 5.5f, 2.4f, 3.8f, 1.1f},
   							   {6.3f, 3.3f, 6.f , 2.5f, 6.3f, 3.3f, 6.f , 2.5f}}; 
        
        Matrix<double[]> matrix = new Matrix<>(features);		
       
        //double x [] = {5.1, 3.5, 1.4, 0.2, 5.1, 3.5, 1.4, 0.2};          
        
        Models<double[]> model = new Models<>(double[].class, modelData); 
        model.predict(matrix).print();        
        
	}
	
}
