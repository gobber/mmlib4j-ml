package mmlib4j.models.transfer;

import mmlib4j.models.datastruct.Matrix;

public class Softmax<N> extends Transfer<N> {

	@Override
	public Matrix<N> activate(Matrix<N> z) {					
		Matrix<N> max = z.max(1);				
		Matrix<N> sum = z.minusi(max).expi().sum(1);						
		return z.divi(sum);
	}	
	
}
