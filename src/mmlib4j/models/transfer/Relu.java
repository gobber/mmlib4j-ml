package mmlib4j.models.transfer;

import mmlib4j.models.datastruct.Matrix;

public class Relu<N> extends Transfer<N>{

	@Override
	public Matrix<N> activate(Matrix<N> z) {
        return z.maxi(0);        
	}	
	
}
