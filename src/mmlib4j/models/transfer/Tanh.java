package mmlib4j.models.transfer;

import mmlib4j.models.datastruct.Matrix;

public class Tanh<N> extends Transfer<N>{
	
	@Override
	public Matrix<N> activate(Matrix<N> z) {			
        return z.tanhi();        
	}
	
}
