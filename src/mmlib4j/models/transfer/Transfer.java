package mmlib4j.models.transfer;

import mmlib4j.models.datastruct.Matrix;

public class Transfer<N> {			
	
	Transfer<N> transfer;
	
	public Transfer() {}
	
	@SuppressWarnings("unchecked")
	public Transfer(Class<N> type, String ac){
		switch (type.getCanonicalName()) {
		case "double[]":			
			if(ac.equalsIgnoreCase("Logistic")) {
				transfer = (Transfer<N>) new Logistic<double[]>();	
			} else if(ac.equalsIgnoreCase("Relu")){
				transfer = (Transfer<N>) new Relu<double[]>();
			} else if(ac.equalsIgnoreCase("Identity")) {
				transfer = (Transfer<N>) new Identity<double[]>();
			} else if(ac.equalsIgnoreCase("Softmax")) {
				transfer = (Transfer<N>) new Softmax<double[]>();
			}else if(ac.equalsIgnoreCase("Tanh")) {
				transfer = (Transfer<N>) new Tanh<double[]>();
			} else {
				throw new IllegalArgumentException("Illegal transfer type.");
			}						
			break;
		case "float[]":
			if(ac.equalsIgnoreCase("Logistic")) {
				transfer = (Transfer<N>) new Logistic<float[]>();	
			} else if(ac.equalsIgnoreCase("Relu")){
				transfer = (Transfer<N>) new Relu<float[]>();
			} else if(ac.equalsIgnoreCase("Identity")) {
				transfer = (Transfer<N>) new Identity<float[]>();
			} else if(ac.equalsIgnoreCase("Softmax")) {
				transfer = (Transfer<N>) new Softmax<float[]>();
			}else if(ac.equalsIgnoreCase("Tanh")) {
				transfer = (Transfer<N>) new Tanh<float[]>();
			}	else {
				throw new IllegalArgumentException("Illegal transfer type.");
			}
			break;
		default:
			throw new IllegalArgumentException("Not allowed data type.");
		}			
	}
	
	public Matrix<N> activate(Matrix<N> z){
		return transfer.activate(z);
	}
	
	public static void main(String args[]) {
		
		double[][] features = {{5.1f, 3.5f, 1.4f, 0.2f, 5.1f, 3.5f, 1.4f, 0.2f}, 
				   {5.9f, 3.f , 5.1f, 1.8f, 5.9f, 3.f , 5.1f, 1.8f},
				   {5.5f, 2.4f, 3.8f, 1.1f, 5.5f, 2.4f, 3.8f, 1.1f},
				   {6.3f, 3.3f, 6.f , 2.5f, 6.3f, 3.3f, 6.f , 2.5f}}; 

		Matrix<double[]> matrix = new Matrix<>(features);
		
		Transfer<double[]> transfer = new Transfer<>(double[].class, "Relu");
		transfer.activate(matrix).print();

		
	}
	
}
