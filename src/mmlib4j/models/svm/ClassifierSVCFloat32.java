package mmlib4j.models.svm;

public class ClassifierSVCFloat32 implements ClassifierSVC<float[]>{
	int nClasses;
    int nRows;
    float[] vectors;
    int numRowsV;
    int numColumnsV;
    float[] coefficients;
    int numRowsC;
    int numColumnsC;
    float[] intercepts;       
    int[] weights;
    String kernel;
    float gamma;
    float coef0;
    float degree;
	@Override
	public int nClasses() {	
		return nClasses;
	}

	@Override
	public int nRows() {
		return nRows;
	}

	@Override
	public float[] vectors() {
		return vectors;
	}

	@Override
	public int numRowsV() {
		return numRowsV;
	}

	@Override
	public int numColumnsV() {
		return numColumnsV;
	}

	@Override
	public float[] coefficients() {
		return coefficients;
	}

	@Override
	public int numRowsC() {
		return numRowsC;
	}

	@Override
	public int numColumnsC() {
		return numColumnsC;
	}

	@Override
	public float[] intercepts() {
		return intercepts;
	}

	@Override
	public int[] weights() {
		return weights;
	}

	@Override
	public String kernel() {
		return kernel;
	}

	@Override
	public Number gamma() {
		return gamma;
	}

	@Override
	public Number coef0() {
		return coef0;
	}

	@Override
	public Number degree() {
		return degree;
	}

	@Override
	public int interceptsLenght() {
		return intercepts.length;
	}

}
