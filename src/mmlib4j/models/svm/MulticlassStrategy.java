package mmlib4j.models.svm;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.preprocessing.Scaler;
import mmlib4j.models.svm.kernels.Kernel;

public interface MulticlassStrategy<N> {
	
	public MulticlassStrategy<N> load(ClassifierSVC<N> clf, Matrix<N> vectors, Matrix<N> coefficients, Scaler<N> scaler, Kernel<N> K, Matrix<N> intercepts, int[] classes);

	public Matrix<N> predict(Matrix<N> x);
	
}
