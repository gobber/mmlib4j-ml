package mmlib4j.models.svm;

public interface ClassifierSVC<N> {
	int nClasses();
    int nRows();
    N vectors();
    int numRowsV();
    int numColumnsV();
    N coefficients();
    int numRowsC();
    int numColumnsC();
    N intercepts();       
    int[] weights();
    String kernel();
    Number gamma();
    Number coef0();
    Number degree();
    int interceptsLenght();
}
