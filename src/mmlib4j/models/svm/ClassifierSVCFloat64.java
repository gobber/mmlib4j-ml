package mmlib4j.models.svm;

public class ClassifierSVCFloat64 implements ClassifierSVC<double[]>{
	int nClasses;
    int nRows;
    double[] vectors;
    int numRowsV;
    int numColumnsV;
    double[] coefficients;
    int numRowsC;
    int numColumnsC;
    double[] intercepts;       
    int[] weights;
    String kernel;
    double gamma;
    double coef0;
    double degree;
	@Override
	public int nClasses() {	
		return nClasses;
	}

	@Override
	public int nRows() {
		return nRows;
	}

	@Override
	public double[] vectors() {
		return vectors;
	}

	@Override
	public int numRowsV() {
		return numRowsV;
	}

	@Override
	public int numColumnsV() {
		return numColumnsV;
	}

	@Override
	public double[] coefficients() {
		return coefficients;
	}

	@Override
	public int numRowsC() {
		return numRowsC;
	}

	@Override
	public int numColumnsC() {
		return numColumnsC;
	}

	@Override
	public double[] intercepts() {
		return intercepts;
	}

	@Override
	public int[] weights() {
		return weights;
	}

	@Override
	public String kernel() {
		return kernel;
	}

	@Override
	public Number gamma() {
		return gamma;
	}

	@Override
	public Number coef0() {
		return coef0;
	}

	@Override
	public Number degree() {
		return degree;
	}

	@Override
	public int interceptsLenght() {
		return intercepts.length;
	}

}
