package mmlib4j.models.svm;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.preprocessing.Scaler;
import mmlib4j.models.svm.kernels.Kernel;

public class MulticlassSVC<N> extends SVC<N> {
	
	private MulticlassStrategy<N> strategy;
	private ClassifierSVC<N> classifier;
	private Matrix<N> vectors;
	private Matrix<N> coefficients;
	private Scaler<N> scaler;	
	private Kernel<N> K;
	private Matrix<N> intercepts;
	private Class<N> modelType;
	private int[] classes;
	
	public MulticlassSVC() {}
	
	public MulticlassSVC (Class<N> modelType, JsonObject jsonObject) {
		this.modelType = modelType;
		load(jsonObject);
	}   
	
    public MulticlassSVC(Class<N> modelType, String modelData) {
    	this.modelType = modelType;
    	load(modelData);
    }    
    
    @SuppressWarnings("unchecked")
	public MulticlassSVC<N> load(JsonObject jsonObject) {
    	
    	switch (modelType.getCanonicalName()) {
		case "double[]":						
			classifier = (ClassifierSVC<N>) new Gson().fromJson(jsonObject, ClassifierSVCFloat64.class);			
			strategy = (MulticlassStrategy<N>) new OneVsOneFloat64();
			break;
		case "float[]":
			classifier = (ClassifierSVC<N>) new Gson().fromJson(jsonObject, ClassifierSVCFloat32.class);
			strategy = (MulticlassStrategy<N>) new OneVsOneFloat32();
			break;
    	}	
		
        this.classes = new int[classifier.nClasses()];        
        for (int i = 0; i < classifier.nClasses(); i++) {
            this.classes[i] = i;
        }                
        K = new Kernel<>(modelType, classifier.kernel(), classifier.gamma(), classifier.degree(), classifier.coef0());
        
        this.vectors      = new Matrix<>(modelType, classifier.numRowsV(), classifier.numColumnsV(), classifier.vectors(), false);	
        this.coefficients = new Matrix<>(modelType, classifier.numRowsC(), classifier.numColumnsC(), classifier.coefficients(), false);
        this.intercepts   = new Matrix<>(modelType, classifier.interceptsLenght(), 1, classifier.intercepts(), false);
        this.scaler = new Scaler<>(modelType, jsonObject);   
        this.strategy.load(classifier, vectors, coefficients, scaler, K, intercepts, classes);
        return this;
    }
    
    public MulticlassSVC<N> load(String modelData) {    	
    	Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
		String jsonStr = scanner.useDelimiter("\\Z").next();   	
		JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject(); 		
		load(jsonObject);
        return this;
    }
    
	@Override
	public Matrix<N> predict(Matrix<N> x) {
		return strategy.predict(x);
	}
	
}
