package mmlib4j.models.svm;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.preprocessing.Scaler;
import mmlib4j.models.svm.kernels.Kernel;

public class OneVsOneFloat32 implements MulticlassStrategy<float[]>{

	private ClassifierSVC<float[]> clf;
	private Matrix<float[]> vectors;
	private Matrix<float[]> coefficients;
	private Scaler<float[]> scaler;	
	private Kernel<float[]> K;
	private Matrix<float[]> intercepts;
	private int[] classes;
	
	public OneVsOneFloat32() {}
	
	@Override
	public MulticlassStrategy<float[]> load(ClassifierSVC<float[]> clf, Matrix<float[]> vectors,
			Matrix<float[]> coefficients, Scaler<float[]> scaler, Kernel<float[]> K, Matrix<float[]> intercepts,
			int[] classes) {
		this.clf = clf;
		this.vectors = vectors;
		this.coefficients = coefficients;
		this.scaler = scaler;
		this.K = K;
		this.intercepts = intercepts;
		this.classes = classes;
		return this;
	}
	
	@Override
	public Matrix<float[]> predict(Matrix<float[]> x) {		
		Matrix<float[]> predict = new Matrix<>(float[].class, x.numRows(), 1);
		Matrix<float[]> trick = K.trick(scaler.normalize(x), vectors);		
		int p = 0;    	
		for(int col = 0; col < trick.numColumns() ; col++) {
	     int[] starts = new int[clf.nRows()];
	        for (int i = 1; i < clf.nRows(); i++) {
	            int start = 0;
	            for (int j = 0; j < i; j++) {
	               start += clf.weights()[j];
	            }
	            starts[i] = start;            
	        }
	        int[] ends = new int[clf.nRows()];
	        for (int i = 0; i < clf.nRows(); i++) {
	            ends[i] = clf.weights()[i] + starts[i];
	        }      

	        int[] histogram = new int[clf.nClasses()];
	        for (int i = 0, d = 0, l = clf.nRows(); i < l; i++) {
	            for (int j = i + 1; j < l; j++) {
	                float tmp = 0;
	                for (int k = starts[j]; k < ends[j]; k++) {	                		                	
	                    tmp += coefficients.getData()[i + k * coefficients.numRows()] * trick.getData()[k + col * trick.numRows()];	                   	                   
	                }
	                for (int k = starts[i]; k < ends[i]; k++) {	                		                	
	                    tmp += coefficients.getData()[(j-1) + k * coefficients.numRows()]  * trick.getData()[k + col * trick.numRows()];	                    	                    
	                }   	                	                
	                histogram[tmp + intercepts.getData()[d] > 0 ? i : j] += 1;	               
	                d++;
	            }
	        }    
	        int classVal = -1, classIdx = -1;
	        for (int i = 0, l = histogram.length; i < l; i++) {
	            if (histogram[i] > classVal) {
	                classVal = histogram[i];
	                classIdx = i;
	            }
	        }	        
	        predict.getData()[p] = classes[classIdx];
			p++;	        
		}
	    return predict;
		
	}
	
	
}
