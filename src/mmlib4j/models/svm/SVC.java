package mmlib4j.models.svm;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.Models;
import mmlib4j.models.datastruct.Matrix;

public class SVC<N> extends Models<N> {

	private Class<N> modelType;
	private SVC<N> svc;
	
	public SVC(){}
	
	public SVC(Class<N> modelType, String modelData){
		this.modelType = modelType;
		load(modelData);
	}
	
	public SVC(Class<N> modelType, JsonObject jsonObject){
		this.modelType = modelType;
		load(jsonObject);
	}
	
	public SVC<N> load(String modelData){
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();        
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();        
        load(jsonObject);                       
        return this;
	}
	
	public SVC<N> load(JsonObject jsonObject) {		
		String type = jsonObject.get("type").getAsString();	
		if(type.contains("SVCBinary")) {
			svc = new BinarySVC<>(modelType, jsonObject);
        }else if(type.contains("SVCMulticlass")) {
        	svc = new MulticlassSVC<>(modelType, jsonObject);
        } else {
        	throw new IllegalArgumentException("Invalid model type");
        }       		
		return this;
	}
	
	@Override
	public Matrix<N> predict(Matrix<N> x) {
		return svc.predict(x);
	}
	
	public static void main(String args[]) {
		
		// Parameters:
        String modelData = "/Users/charles/Desktop/data.json";
        
        float[][] features = {{5.1f, 3.5f, 1.4f, 0.2f, 5.1f, 3.5f, 1.4f, 0.2f}, 
        					   {5.9f, 3.f , 5.1f, 1.8f, 5.9f, 3.f , 5.1f, 1.8f},
   							   {5.5f, 2.4f, 3.8f, 1.1f, 5.5f, 2.4f, 3.8f, 1.1f},
   							   {6.3f, 3.3f, 6.f , 2.5f, 6.3f, 3.3f, 6.f , 2.5f}}; 
        
        Matrix<float[]> matrix = new Matrix<>(features);		
        
        SVC<float[]> svc = new SVC<>(float[].class, modelData);
        svc.predict(matrix).print();
                
	}
	
}
