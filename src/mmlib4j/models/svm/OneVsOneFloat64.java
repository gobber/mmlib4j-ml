package mmlib4j.models.svm;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.preprocessing.Scaler;
import mmlib4j.models.svm.kernels.Kernel;

public class OneVsOneFloat64 implements MulticlassStrategy<double[]>{

	private ClassifierSVC<double[]> clf;
	private Matrix<double[]> vectors;
	private Matrix<double[]> coefficients;
	private Scaler<double[]> scaler;	
	private Kernel<double[]> K;
	private Matrix<double[]> intercepts;
	private int[] classes;
	
	public OneVsOneFloat64() {}
	
	@Override
	public MulticlassStrategy<double[]> load(ClassifierSVC<double[]> clf, Matrix<double[]> vectors,
			Matrix<double[]> coefficients, Scaler<double[]> scaler, Kernel<double[]> K, Matrix<double[]> intercepts,
			int[] classes) {
		this.clf = clf;
		this.vectors = vectors;
		this.coefficients = coefficients;
		this.scaler = scaler;
		this.K = K;
		this.intercepts = intercepts;
		this.classes = classes;
		return this;
	}
	
	@Override
	public Matrix<double[]> predict(Matrix<double[]> x) {		
		Matrix<double[]> predict = new Matrix<>(double[].class, x.numRows(), 1);
		Matrix<double[]> trick = K.trick(scaler.normalize(x), vectors);		
		int p = 0;    	
		for(int col = 0; col < trick.numColumns() ; col++) {
	     int[] starts = new int[clf.nRows()];
	        for (int i = 1; i < clf.nRows(); i++) {
	            int start = 0;
	            for (int j = 0; j < i; j++) {
	               start += clf.weights()[j];
	            }
	            starts[i] = start;            
	        }
	        int[] ends = new int[clf.nRows()];
	        for (int i = 0; i < clf.nRows(); i++) {
	            ends[i] = clf.weights()[i] + starts[i];
	        }      

	        int[] histogram = new int[clf.nClasses()];
	        for (int i = 0, d = 0, l = clf.nRows(); i < l; i++) {
	            for (int j = i + 1; j < l; j++) {
	                double tmp = 0.0;
	                for (int k = starts[j]; k < ends[j]; k++) {	                		                	
	                    tmp += coefficients.getData()[i + k * coefficients.numRows()] * trick.getData()[k + col * trick.numRows()];	                   	                   
	                }
	                for (int k = starts[i]; k < ends[i]; k++) {	                		                	
	                    tmp += coefficients.getData()[(j-1) + k * coefficients.numRows()]  * trick.getData()[k + col * trick.numRows()];	                    	                    
	                }   	                	                
	                histogram[tmp + intercepts.getData()[d] > 0 ? i : j] += 1;	               
	                d++;
	            }
	        }    
	        int classVal = -1, classIdx = -1;
	        for (int i = 0, l = histogram.length; i < l; i++) {
	            if (histogram[i] > classVal) {
	                classVal = histogram[i];
	                classIdx = i;
	            }
	        }	        
	        predict.getData()[p] = classes[classIdx];
			p++;	        
		}
	    return predict;
		
	}
	
	
}
