package mmlib4j.models.svm;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.preprocessing.Scaler;
import mmlib4j.models.svm.kernels.Kernel;

public final class BinarySVC<N> extends SVC<N> {
	
	private Scaler<N> scaler;	
	private Matrix<N> vectors;
    private Matrix<N> intercepts;
    private Matrix<N> coefficients;    
    private Kernel<N> K;
    private Class<N> modelType;
    private ClassifierSVC<N> classifier;
	
	public BinarySVC() {}
	
	public BinarySVC(Class<N> modelType, JsonObject jsonObject) {
		this.modelType = modelType;
		load(jsonObject);
	}
	
    public BinarySVC(Class<N> modelType, String modelData) {
        this.modelType = modelType;
        load(modelData);
    }
    
    public BinarySVC<N> load(String modelData){
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();        
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();        
        load(jsonObject);                       
        return this;
	}
    
    @SuppressWarnings("unchecked")
	public BinarySVC<N> load(JsonObject jsonObject) {    	
    	switch (modelType.getCanonicalName()) {
		case "double[]":					
			classifier = (ClassifierSVC<N>) new Gson().fromJson(jsonObject, ClassifierSVCFloat64.class);										
			break;
		case "float[]":
			classifier = (ClassifierSVC<N>) new Gson().fromJson(jsonObject, ClassifierSVCFloat32.class);						
			break;
    	}					                
        this.K = new Kernel<>(modelType, classifier.kernel(), classifier.gamma(), classifier.degree(), classifier.coef0());        
        this.vectors      = new Matrix<>(modelType, classifier.numRowsV(), classifier.numColumnsV(), classifier.vectors(), false);        
        this.coefficients = new Matrix<>(modelType, classifier.numRowsC(), classifier.numColumnsC(), classifier.coefficients(), false);        
        this.intercepts   = new Matrix<>(modelType, classifier.interceptsLenght(), 1, classifier.intercepts(), false);        
        this.scaler = new Scaler<>(modelType, jsonObject);        
        return this;
    }
    
	@Override
	public Matrix<N> predict(Matrix<N> x) {
		return K.trick(scaler.normalize(x), vectors).TdotT(-1, coefficients).plusi(intercepts).lesstheni(0);    	
	}
	
}
