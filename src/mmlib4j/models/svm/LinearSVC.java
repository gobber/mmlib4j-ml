package mmlib4j.models.svm;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.Models;
import mmlib4j.models.datastruct.Matrix;
import mmlib4j.models.postprocessing.PostProcessing;
import mmlib4j.models.postprocessing.PostProcessingLinear;
import mmlib4j.models.postprocessing.PostProcessingMulticlass;
import mmlib4j.models.preprocessing.Scaler;

public class LinearSVC<N> extends Models<N> {		
	
    private interface Classifier<N>{
    	N coefficients();
    	N intercepts();
    	int numRowsC();
    	int numColumnsC();
    	int interceptsLength();
    }
    
    private class ClassifierFloat64 implements Classifier<double[]>{    	
    	double coefficients[];
    	double intercepts[];
    	int numRowsC;
    	int numColumnsC;    	
		@Override
		public double[] coefficients() {
			return coefficients;
		}
		@Override
		public double[] intercepts() {
			return intercepts;
		}
		@Override
		public int numRowsC() {
			return numRowsC;
		}
		@Override
		public int numColumnsC() {
			return numColumnsC;
		}
		@Override
		public int interceptsLength() {
			return intercepts.length;
		}    	
    }
    
    private class ClassifierFloat32 implements Classifier<float[]>{    	
    	float coefficients[];
    	float intercepts[];
    	int numRowsC;
    	int numColumnsC;    	
		@Override
		public float[] coefficients() {
			return coefficients;
		}
		@Override
		public float[] intercepts() {
			return intercepts;
		}
		@Override
		public int numRowsC() {
			return numRowsC;
		}
		@Override
		public int numColumnsC() {
			return numColumnsC;
		}
		@Override
		public int interceptsLength() {
			return intercepts.length;
		}    	
    }
    
    private Classifier<N> classifier;
    private PostProcessing<N> post;
    private Class<N> modelType;
    private Matrix<N> coefficients;
    private Matrix<N> intercepts;
    private Scaler<N> scaler;
    
    public LinearSVC(){}
	
	public LinearSVC(Class<N> modelType, String modelData){
		this.modelType = modelType;
		load(modelData);
	}
	
	public LinearSVC(Class<N> modelType, JsonObject jsonObject){
		this.modelType = modelType;
		load(jsonObject);
	}
    
    @SuppressWarnings("unchecked")
	public LinearSVC<N> load(JsonObject jsonObject) {   
    	
    	switch (modelType.getCanonicalName()) {
		case "double[]":					
			this.classifier = (Classifier<N>) new Gson().fromJson(jsonObject, ClassifierFloat64.class);										
			break;
		case "float[]":
			this.classifier = (Classifier<N>) new Gson().fromJson(jsonObject, ClassifierFloat32.class);						
			break;
		}		      	
    	
        coefficients = new Matrix<>(modelType, classifier.numRowsC(), classifier.numColumnsC(), classifier.coefficients(), false);
        intercepts   = new Matrix<>(modelType, 1, classifier.interceptsLength(), classifier.intercepts(), false);
        scaler = new Scaler<>(modelType, jsonObject);      
        
        String type = jsonObject.get("type").getAsString();
        
        if(type.contains("LinearSVCBinary")) {
        	post = new PostProcessingLinear<>();
        }else if(type.contains("LinearSVCMulticlass")) {
        	post = new PostProcessingMulticlass<>();
        } else {
        	throw new IllegalArgumentException("Invalid model type");
        }  
        
        return this;
    }
    
    public LinearSVC<N> load(String modelData) {    	
    	Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
        String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();
        JsonParser parser = new JsonParser();       
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();                
        return load(jsonObject);
    }        	

    @Override
	public Matrix<N> predict(Matrix<N> x) {
		return post.execute(scaler.normalize(x).dotT(coefficients).plusi(intercepts));
	}
    
    public static void main(String args[]) {
		
		// Parameters:
        String modelData = "/Users/charles/Desktop/data.json";
        
        float[][] features = {{5.1f, 3.5f, 1.4f, 0.2f, 5.1f, 3.5f, 1.4f, 0.2f}, 
        					   {5.9f, 3.f , 5.1f, 1.8f, 5.9f, 3.f , 5.1f, 1.8f},
   							   {5.5f, 2.4f, 3.8f, 1.1f, 5.5f, 2.4f, 3.8f, 1.1f},
   							   {6.3f, 3.3f, 6.f , 2.5f, 6.3f, 3.3f, 6.f , 2.5f}}; 
        
        Matrix<float[]> matrix = new Matrix<>(features);		
        
        LinearSVC<float[]> svc = new LinearSVC<>(float[].class, modelData);
        svc.predict(matrix).print();
                
	}
    
}
