package mmlib4j.models.svm.kernels;
import mmlib4j.models.datastruct.Matrix;

public class Rbf<N> extends Kernel<N> {

	private Number gamma;
	private Class<N> type;
	
	public Rbf(Class<N> type, Number gamma) {
		this.gamma = gamma;
		this.type = type;
	}

	@Override
	public Matrix<N> trick(Matrix<N> x, Matrix<N> vectors) {		
		Matrix<N> vsq = vectors.pow2().sumn(1);
        Matrix<N> xsq = x.pow2().sumn(1);                
        Matrix<N> aux = new Matrix<>(type, x.numRows(), 1);
        aux.one();
        
        Matrix<N> sum = vsq.dotT(aux);        
        Matrix<N> aux2 = new Matrix<>(type, vectors.numRows(), 1);
        aux2.one();
        
        aux2.dotT(xsq, sum);        
        vectors.dotT(-2, x, sum);        
        return sum.muli(gamma).negi().expi();        		
	}

}
