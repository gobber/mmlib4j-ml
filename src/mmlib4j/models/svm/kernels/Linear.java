package mmlib4j.models.svm.kernels;
import mmlib4j.models.datastruct.Matrix;

public class Linear<N> extends Kernel<N>{

	@Override
	public Matrix<N> trick(Matrix<N> x, Matrix<N> vectors) {
		return vectors.dotT(x);
	}

}
