package mmlib4j.models.svm.kernels;
import mmlib4j.models.datastruct.Matrix;

public class Kernel<N> {
	
	private Kernel<N> kernel;
	
	Kernel(){}
	
	public Kernel (Class<N> type, String KernelType, Number gamma, Number degree, Number coef0) {	
		switch(KernelType.toUpperCase()){		
			case "LINEAR":
				kernel = new Linear<>();
				break;
			case "POLY":
				kernel = new Poly<>(gamma, degree, coef0);
				break;
			case "SIGMOID":
				kernel = new Sigmoid<>(gamma, coef0);
				break;
        	case "RBF":
        		kernel = new Rbf<>(type, gamma);
        		break;
			default:
				throw new IllegalArgumentException("Invalid kernel type");		
		}			
	}
	
	public Matrix<N> trick(Matrix<N> x, Matrix<N> vectors){
		return kernel.trick(x, vectors);
	}

}
