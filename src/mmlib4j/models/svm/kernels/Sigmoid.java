package mmlib4j.models.svm.kernels;
import mmlib4j.models.datastruct.Matrix;

public class Sigmoid<N> extends Kernel<N>{

	private Number coef0;
	private Number gamma;
	
	public Sigmoid(Number gamma, Number coef0) {
		this.gamma  = gamma;
		this.coef0  = coef0;
	}

	@Override
	public Matrix<N> trick(Matrix<N> x, Matrix<N> vectors) {
		return vectors.dotT(gamma, x).plusi(coef0).tanhi();
	}	
	
}
