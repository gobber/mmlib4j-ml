package mmlib4j.models.svm.kernels;
import mmlib4j.models.datastruct.Matrix;

public class Poly<N> extends Kernel<N>{
	
	private Number coef0;
	private Number gamma;
	private Number degree;

	public Poly(Number gamma, Number degree, Number coef0) {
		this.gamma  = gamma;
		this.degree = degree;
		this.coef0  = coef0;
	}

	@Override
	public Matrix<N> trick(Matrix<N> x, Matrix<N> vectors) {
		return vectors.dotT(gamma, x).plusi(coef0).powi(degree);
	}


}
