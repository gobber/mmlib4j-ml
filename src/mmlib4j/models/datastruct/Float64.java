package mmlib4j.models.datastruct;
import java.util.Arrays;

public class Float64 extends AbstractMatrix<double[]>{

	public static final Float64 instance = new Float64();		
	
	@Override
	public void print(Matrix<double[]> source) {
		for(int row = 0 ; row < source.numRows() ; row++) {
  			for(int col = 0 ; col < source.numColumns() ; col++) {
  				System.out.printf("[%e] ", source.getData()[getIndex(row, col, source.numRows())]);
  			}    		
  			System.out.println();
  		}   	
	}

	@Override
	public Matrix<double[]> one(Matrix<double[]> A) {
		fill(A, 1);
		return A;
	}

	@Override
	public Matrix<double[]> zero(Matrix<double[]> A) {
		fill(A, 0);
		return A;
	}
	
	@Override
	public Matrix<double[]> fill(Matrix<double[]> source, Number value) {
		Arrays.fill(source.getData(), value.doubleValue());
		return source;
	}	
	
	@Override 
	public Matrix<double[]> copy(Matrix<double[]> source){
		Matrix<double[]> matrix = new Matrix<double[]>(source.numRows(), source.numColumns(), source.getData(), true);
		return matrix;
	}

	@Override
	public double[] create(int numRows, int numColumns) {
		return new double[numRows*numColumns];
	}
	
	@Override
	public void head(Matrix<double[]> source, int numRows, int numColumns, String format) {
		numRows = numRows       > source.numRows()    ? source.numRows()    : numRows;
		numColumns = numColumns > source.numColumns() ? source.numColumns() : numColumns;	
		for(int row = 0 ; row < numRows ; row++) { 
			for(int col= 0 ; col < numColumns ; col++)
				System.out.format(format, source.getData()[getIndex(row, col, numRows)]);
			System.out.println();			
		}	
	}
	
	@Override
	public String toString(Matrix<double[]> source, String separator) {		
		String out = "";					
		for(int row = 0 ; row < source.numRows() ; row++)
			for(int col = 0 ; col < source.numColumns() ; col++)
				out += source.getData()[getIndex(row, col, source.numRows())] + separator;						
		return out.substring(0, out.length()-1);		
	}

	@Override
	public Matrix<double[]> row(Matrix<double[]> A, int row) {
		Matrix<double[]> C = new Matrix<>(double[].class, 1, A.numColumns());		
		return row(A, row, C);
	}

	@Override
	public Matrix<double[]> row(Matrix<double[]> A, int row, Matrix<double[]> C) {
		for (int col = 0; col < A.numColumns(); col++)
			C.getData()[getIndex(0, col, 1)] = A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	public Matrix<double[]> column(Matrix<double[]> A, int column) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), 1);		
		return row(A, column, C);
	}

	@Override
	public Matrix<double[]> column(Matrix<double[]> A, int column, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++)
			C.getData()[getIndex(row, 0, 1)] = A.getData()[getIndex(row, column, A.numRows())];
		return C;
	}

	@Override
	public Matrix<double[]> biggerthen(Matrix<double[]> A, Number t) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());		
		return biggerthen(A, t, C);
	}

	@Override
	public Matrix<double[]> biggerthen(Matrix<double[]> A, Number t, Matrix<double[]> C) {
		double rt = t.doubleValue();		
		for (int row = 0; row < A.numRows(); row++) 
	        for (int col = 0; col < A.numColumns(); col++)
	        	if(A.getData()[getIndex(row, col, A.numRows())] > rt)
	        		C.getData()[getIndex(row, col, C.numRows())] = 1;
	        	else
	        		C.getData()[getIndex(row, col, C.numRows())] = 0;
	        		
		return C;
	}

	@Override
	public Matrix<double[]> lessthen(Matrix<double[]> A, Number t) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());	
		return lessthen(A, t, C);
	}

	@Override
	public Matrix<double[]> lessthen(Matrix<double[]> A, Number t, Matrix<double[]> C) {
		double rt = t.doubleValue();		
		for (int row = 0; row < A.numRows(); row++) 
	        for (int col = 0; col < A.numColumns(); col++)
	        	if(A.getData()[getIndex(row, col, A.numRows())] < rt)
	        		C.getData()[getIndex(row, col, C.numRows())] = 1;
	        	else
	        		C.getData()[getIndex(row, col, C.numRows())] = 0;
	        		
		return C;
	}

	@Override
	public Matrix<double[]> threshold(Matrix<double[]> A, Number t) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());	
		return threshold(A, t, C);
	}

	@Override
	public Matrix<double[]> threshold(Matrix<double[]> A, Number t, Matrix<double[]> C) {
		return biggerthen(A, t, C);
	}
	
	@Override
	public Matrix<double[]> sum(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, 1, 1); 
		return sum(A, 1, 1, C);
	}

	@Override
	public Matrix<double[]> sum(Matrix<double[]> A, int axis) {
		Matrix<double[]> C;
    	if(axis == 0) {    		
    		C = new Matrix<>(double[].class, 1, A.numColumns());
    		return sum(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){    
    		C = new Matrix<>(double[].class, A.numRows(), 1);
    		return sum(A, A.numRows(), 1, C);    		
    	}else {			
			 throw new IllegalArgumentException("Invalid axis");			
		}		    	    	
	}

	public Matrix<double[]> sum(Matrix<double[]> A, int r, int c, Matrix<double[]> C){		
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)             	            	
            	C.getData()[getIndex(row%r, col%c, C.numRows())] = C.getData()[getIndex(row%r, col%c, C.numRows())] + A.getData()[getIndex(row, col, A.numRows())];            	         
		return C;    
	}

	@Override
	public Matrix<double[]> sumn(Matrix<double[]> A) {
		Matrix<double[]> aux1 = new Matrix<>(double[].class, 1, A.numRows());
		aux1.one();
		Matrix<double[]> aux2 = new Matrix<>(double[].class, 1, A.numColumns());
		Matrix<double[]> aux3 = new Matrix<>(double[].class, A.numColumns(), 1);
		aux3.one();
		Matrix<double[]> C = new Matrix<>(double[].class, 1, 1);			
		Algebra.multAdd(1, aux1, A, aux2);
		Algebra.multAdd(1, aux2, aux3, C);						
		return C;		
	}

	@Override
	public Matrix<double[]> sumn(Matrix<double[]> A, int axis) {
		Matrix<double[]> B;
		Matrix<double[]> C;		
		if(axis == 0) {
			B = new Matrix<>(double[].class, 1, A.numRows());
			B.one();
			C = new Matrix<>(double[].class, 1, A.numColumns());
			Algebra.multAdd(1, B, A, C);
			return C;
		} else if(axis == 1) {
			B = new Matrix<>(double[].class, A.numColumns(), 1);
			B.one();
			C = new Matrix<>(double[].class, A.numRows(), 1);
			Algebra.multAdd(1, A, B, C);
			return C;
		} else {			
			throw new IllegalArgumentException("Invalid axis");			
		}
	}

	@Override
	public Matrix<double[]> sumn(Matrix<double[]> A, int axis, Matrix<double[]> C) {
		Matrix<double[]> B;
		if(axis == 0) {
			B = new Matrix<>(double[].class, 1, A.numRows());
			B.one();
			Algebra.multAdd(1, B, A, C);
			return C;
		} else if(axis == 1) {
			B = new Matrix<>(double[].class, A.numColumns(), 1);
			B.one();
			Algebra.multAdd(1, A, B, C);
			return C;
		} else {			
			throw new IllegalArgumentException("Invalid axis");			
		}		
	}

	@Override
	public Matrix<double[]> sumn(Matrix<double[]> A, int axis, Matrix<double[]> B, Matrix<double[]> C) {
		if(axis == 0) {
			Algebra.multAdd(1, B, A, C);
			return C;
		} else if(axis == 1) {		
			Algebra.multAdd(1, A, B, C);
			return C;
		} else {			
			throw new IllegalArgumentException("Invalid axis");			
		}		
	}

	@Override
	public Matrix<double[]> T(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numColumns(), A.numRows());
		return T(A, C);
	}

	@Override
	public Matrix<double[]> T(Matrix<double[]> A, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	
            	C.getData()[getIndex(col, row, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())]; 		
		return C;		
	}

	@Override
	public Matrix<double[]> muli(Matrix<double[]> A, Matrix<double[]> B) {
		if(B.isScalar()) {
			return mul(A, B.getData()[0], A);
		}		
		return mul(A, B, A);
	}

	@Override
	public Matrix<double[]> mul(Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {	
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] * B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];  							
		return C;       
	}

	@Override
	public Matrix<double[]> mul(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return mul(A, B.getData()[0], C);
		}		
		return mul(A, B, C);
	}

	@Override
	public Matrix<double[]> mul(Matrix<double[]> A, Number value) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());   			       
		return mul(A, value, C); 
	}

	@Override
	public Matrix<double[]> mul(Matrix<double[]> A, Number value, Matrix<double[]> C) {
		double val = value.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] * val;
		return C;       
	}

	@Override
	public Matrix<double[]> plusni(Matrix<double[]> A, Matrix<double[]> B) {
		Algebra.dplus(A.numRows()*A.numColumns(), A,  B);
		return A;
	}

	@Override
	public Matrix<double[]> plusn(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = A.copy();
		Algebra.dplus(A.numRows()*A.numColumns(), C,  B);
		return C;
	}

	@Override
	public Matrix<double[]> plusi(Matrix<double[]> A, Matrix<double[]> B) {
		if(B.isScalar()) {
			return plus(A, B.getData()[0], A);
		}    		
		return plus(A, B, A);
	}

	@Override
	public Matrix<double[]> plus(Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] + B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];		
		return C;
	}

	@Override
	public Matrix<double[]> plus(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return plus(A, B.getData()[0], C);
		}    		
		return plus(A, B, C);
	}

	@Override
	public Matrix<double[]> plus(Matrix<double[]> A, Number value) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());	   
		return plus(A, value, C);			
	}

	@Override
	public Matrix<double[]> plus(Matrix<double[]> A, Number value, Matrix<double[]> C) {
		double val = value.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] + val;
		return C;
	}

	@Override
	public Matrix<double[]> minusni(Matrix<double[]> A, Matrix<double[]> B) {
		Algebra.dminus(A.numRows()*A.numColumns(), A, B);
		return A;
	}

	@Override
	public Matrix<double[]> minusn(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = A.copy();
		Algebra.dminus(A.numRows()*A.numColumns(), C, B);
		return C;
	}

	@Override
	public Matrix<double[]> minusi(Matrix<double[]> A, Matrix<double[]> B) {
		if(B.isScalar()) {
			return minus(A, B.getData()[0], A);
		}		
        return minus(A, B, A);
	}

	@Override
	public Matrix<double[]> minus(Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] - B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];		
		return C;
	}

	@Override
	public Matrix<double[]> minus(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return minus(A, B.getData()[0], C);
		}    		
		return minus(A, B, C);
	}

	@Override
	public Matrix<double[]> minus(Matrix<double[]> A, Number value) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());	   
		return minus(A, value, C);			
	}

	@Override
	public Matrix<double[]> minus(Matrix<double[]> A, Number value, Matrix<double[]> C) {
		double val = value.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] - val;
		return C;
	}

	@Override
	public Matrix<double[]> rdiv(Matrix<double[]> A, Number value) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());	   
		return rdiv(A, value, C);			
	}

	@Override
	public Matrix<double[]> rdiv(Matrix<double[]> A, Number value, Matrix<double[]> C) {
		double val = value.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = val / A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	public Matrix<double[]> divi(Matrix<double[]> A, Matrix<double[]> B) {
		if(B.isScalar()) {
			return div(A, B.getData()[0], A);
		}    		
		return div(A, B, A);
	}

	@Override
	public Matrix<double[]> div(Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] / B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];		
		return C;
	}

	@Override
	public Matrix<double[]> div(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return div(A, B.getData()[0], C);
		}    		
		return div(A, B, C);
	}

	@Override
	public Matrix<double[]> div(Matrix<double[]> A, Number value) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());	   
		return div(A, value, C);			
	}

	@Override
	public Matrix<double[]> div(Matrix<double[]> A, Number value, Matrix<double[]> C) {
		double val = value.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] / val;
		return C;
	}

	@Override
	public Matrix<double[]> dot(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), B.numColumns());
		Algebra.multAdd(1, A, B, C);
		return C;
	}

	@Override
	public Matrix<double[]> dot(Matrix<double[]> A, Number value, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), B.numColumns());
		return Algebra.multAdd(value.doubleValue(), A, B, C);
	}

	@Override
	public Matrix<double[]> dot(Matrix<double[]> A, Number value, Matrix<double[]> B, Matrix<double[]> C) {
		return Algebra.multAdd(value.doubleValue(), A, B, C);
	}
	
	
	
	@Override
	public Matrix<double[]> Tdot(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numColumns(), B.numColumns());
		return Tdot(A, B, C);
	}

	@Override
	Matrix<double[]> Tdot(Matrix<double[]> A, Number value, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numColumns(), B.numColumns());		
		Algebra.transAmultAdd(value.doubleValue(), A, B, C);
		return C;
	}

	@Override
	Matrix<double[]> Tdot(Matrix<double[]> A, Number value, Matrix<double[]> B, Matrix<double[]> C) {
		Algebra.transAmultAdd(value.doubleValue(), A, B, C);
		return C;
	}

	@Override
	Matrix<double[]> dotT(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), B.numRows());
		Algebra.transBmultAdd(1, A, B, C);
		return C;
	}

	@Override
	public Matrix<double[]> dotT(Matrix<double[]> A, Number value, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), B.numRows());
		Algebra.transBmultAdd(value.doubleValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<double[]> dotT(Matrix<double[]> A, Number value, Matrix<double[]> B, Matrix<double[]> C) {
		Algebra.transBmultAdd(value.doubleValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<double[]> TdotT(Matrix<double[]> A, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numColumns(), B.numRows());		
		return TdotT(A, B, C);
	}

	@Override
	public Matrix<double[]> TdotT(Matrix<double[]> A, Number value, Matrix<double[]> B) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numColumns(), B.numRows());
		Algebra.transABmultAdd(value.doubleValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<double[]> TdotT(Matrix<double[]> A, Number value, Matrix<double[]> B, Matrix<double[]> C) {
		Algebra.transABmultAdd(value.doubleValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<double[]> neg(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numColumns(), A.numRows());
		return neg(C);
	}

	@Override
	public Matrix<double[]> neg(Matrix<double[]> A, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = -A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	Matrix<double[]> mean(Matrix<double[]> A, int r, int c, Number div, Matrix<double[]> C) {
		sum(A, r, c, C);
		return divi(C, div);
	}
	
	@Override
	public Matrix<double[]> mean(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, 1, 1);
		sum(A, 1, 1, C);
		return C;
	}

	@Override
	public Matrix<double[]> mean(Matrix<double[]> A, int axis, Matrix<double[]> C) {
		if(axis == 0) {
			return mean(A, 1, A.numColumns(), A.numRows(), C);    		
		}else if(axis == 1){
			return mean(A, A.numRows(), 1, A.numColumns(), C);    	
		} else {
			throw new IllegalArgumentException("Invalid axis");
		}    	  
	}

	@Override
	public Matrix<double[]> mean(Matrix<double[]> A, int axis) {
		Matrix<double[]>  C;		
		if(axis == 0) {
			C = new  Matrix<>(double[].class,1, A.numColumns());    		
			return mean(A, 1, A.numColumns(), A.numRows(), C);    		
		}else if(axis == 1){
			C = new  Matrix<>(double[].class, A.numRows(), 1);
			return mean(A, A.numRows(), 1, A.numColumns(), C);    	
		} else {
			throw new IllegalArgumentException("Invalid axis");
		}    	   
	}

	@Override
	public Matrix<double[]> meann(Matrix<double[]> A) {
		Matrix<double[]> C = sumn(A);
		C.getData()[0] = C.getData()[0] / (A.numRows()*A.numColumns());		
		return C;
	}
	
	@Override
	public Matrix<double[]> meann(Matrix<double[]> A, int axis){
		Matrix<double[]> C = sumn(A, axis);
		if(axis == 0)
			C.divi(A.numRows());
		else
			C.divi(A.numColumns());
		return C;
	}

	@Override
	public Matrix<double[]> pow2(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		return pow2(A, C);
	}

	@Override
	public Matrix<double[]> pow2(Matrix<double[]> A, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] * A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	public Matrix<double[]> pow(Matrix<double[]> A, Number exponent) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		return pow(A, exponent,  C);
	}

	@Override
	public Matrix<double[]> pow(Matrix<double[]> A, Number exponent, Matrix<double[]> C) {
		double exp = exponent.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = Math.pow(A.getData()[getIndex(row, col, A.numRows())], exp) ;
		return C;
	}

	@Override
	public Matrix<double[]> sqrt(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		return sqrt(A, C);
	}

	@Override
	public Matrix<double[]> sqrt(Matrix<double[]> A, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = Math.sqrt(A.getData()[getIndex(row, col, A.numRows())]);
		return C;
	}

	@Override
	public Matrix<double[]> exp(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		return exp(A, C);
	}

	@Override
	public Matrix<double[]> exp(Matrix<double[]> A, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = Math.exp(A.getData()[getIndex(row, col, A.numRows())]);
		return C;
	}

	@Override
	public Matrix<double[]> tanh(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		return tanh(A, C);
	}

	@Override
	public Matrix<double[]> tanh(Matrix<double[]> A, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = Math.tanh(A.getData()[getIndex(row, col, A.numRows())]);
		return C;
	} 
	
	@Override
	public Matrix<double[]> var(Matrix<double[]> A, int axis, int d) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		if(axis == 0) {    		
    		return var(A, axis, A.numRows(), d, C);    		
    	}else if(axis == 1){    		
    		return var(A, axis, A.numColumns(), d, C);    		
    	}else {
			throw new IllegalArgumentException("Invalid axis");
		}    	      	
	}

	@Override
	public Matrix<double[]> varn(Matrix<double[]> A, int axis, int d) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns()); 
		if(axis == 0) {
    		return varn(A, axis, A.numRows(), d, C);    		
    	}else if(axis == 1){
    		return varn(A, axis, A.numColumns(), d, C);    	
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}    			
	}

	@Override
	public Matrix<double[]> std(Matrix<double[]> A, int axis, int d) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		return std(A, axis, d, C);
	}

	@Override
	public Matrix<double[]> stdn(Matrix<double[]> A, int axis, int d) {
		Matrix<double[]> C = new Matrix<>(double[].class, A.numRows(), A.numColumns());
		if(axis == 0) {
    		return stdn(A, axis, A.numRows(), d, C);    		
    	}else if(axis == 1){
    		return stdn(A, axis, A.numColumns(), d, C);    	
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}    	 
	}
	
	Matrix<double[]> max(Matrix<double[]> A, int r, int c, Matrix<double[]> C){
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)
            	C.getData()[getIndex(row%r, col%c, C.numRows())] =  Math.max(C.getData()[getIndex(row%r, col%c, C.numRows())], A.getData()[getIndex(row, col, A.numRows())]);                      
		return C;    	
	}

	@Override
	public Matrix<double[]> max(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, 1, 1);
		C.fill(Double.MIN_VALUE);
		return max(A, 1, 1, C);
	}

	@Override
	public Matrix<double[]> max(Matrix<double[]> A, int axis) {
		Matrix<double[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(double[].class, 1, A.numColumns());
    		C.fill(Double.MIN_VALUE);
    		return max(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(double[].class, A.numRows(), 1);
    		C.fill(Double.MIN_VALUE);
    		return max(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}       	
	}

	@Override
	public Matrix<double[]> max(Matrix<double[]> A, Number value, Matrix<double[]> C) {
		double val = value.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	
            	C.getData()[getIndex(row, col, C.numRows())] = Math.max(val, A.getData()[getIndex(row, col, A.numRows())]);            	         
		return C;
	}
	
	Matrix<double[]> min(Matrix<double[]> A, int r, int c, Matrix<double[]> C){
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)
            	C.getData()[getIndex(row%r, col%c, C.numRows())] =  Math.min(C.getData()[getIndex(row%r, col%c, C.numRows())], A.getData()[getIndex(row, col, A.numRows())]);                      
		return C;    	
	}

	@Override
	public Matrix<double[]> min(Matrix<double[]> A) {
		Matrix<double[]> C = new Matrix<>(double[].class, 1, 1);
		C.fill(Double.MAX_VALUE);
		return min(A, 1, 1, C);
	}

	@Override
	public Matrix<double[]> min(Matrix<double[]> A, int axis) {
		Matrix<double[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(double[].class, 1, A.numColumns());
    		C.fill(Double.MAX_VALUE);
    		return min(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(double[].class, A.numRows(), 1);
    		C.fill(Double.MAX_VALUE);
    		return min(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}       
	}

	@Override
	public Matrix<double[]> min(Matrix<double[]> A, Number value, Matrix<double[]> C) {
		double val = value.doubleValue();
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	
            	C.getData()[getIndex(row, col, C.numRows())] = Math.min(val, A.getData()[getIndex(row, col, A.numRows())]);            	         
		return C;
	}

	@Override
	public Matrix<double[]> argmax(Matrix<double[]> A, int axis) {
		Matrix<double[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(double[].class, 1, A.numColumns());
    		return rowargmax(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(double[].class, A.numRows(), 1);
    		return colargmax(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}     
	}
	
	Matrix<double[]> colargmax(Matrix<double[]> A, int r, int c, Matrix<double[]> C) {		
		Matrix<double[]> aux = new Matrix<>(double[].class, r, c);		
		aux.fill(Double.MIN_VALUE);		
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] > aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = col;            		            		
            	}    	    	
		return C;		
    }
	
	Matrix<double[]> rowargmax(Matrix<double[]>A, int r, int c, Matrix<double[]> C) {
		Matrix<double[]> aux = new Matrix<>(double[].class, r, c);		
		aux.fill(Double.MIN_VALUE);
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] > aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = row;            		            		
            	}    	    	
		return C;		   	
    }

	@Override
	public Matrix<double[]> argmin(Matrix<double[]> A, int axis) {
		Matrix<double[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(double[].class, 1, A.numColumns());
    		return rowargmin(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(double[].class, A.numRows(), 1);
    		return colargmin(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}     
	}

	Matrix<double[]> colargmin(Matrix<double[]> A, int r, int c, Matrix<double[]> C) {		
		Matrix<double[]> aux = new Matrix<>(double[].class, r, c);		
		aux.fill(Double.MAX_VALUE);		
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] < aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = col;            		            		
            	}    	    	
		return C;		
    }
	
	Matrix<double[]> rowargmin(Matrix<double[]>A, int r, int c, Matrix<double[]> C) {
		Matrix<double[]> aux = new Matrix<>(double[].class, r, c);		
		aux.fill(Double.MAX_VALUE);
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] < aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = row;            		            		
            	}    	    	
		return C;		   	
    }
	
	@Override
	public Matrix<double[]> sigmoid(Matrix<double[]> A, Matrix<double[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)             	
            	C.getData()[getIndex(row, col, C.numRows())] = 1.0/(1.0 + Math.exp(-A.getData()[getIndex(row, col, A.numRows())]));            			
		return C;
	}

	@Override
	public double[] clone(double[] A) {
		return A.clone();
	}
	
}
