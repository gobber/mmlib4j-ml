package mmlib4j.models.datastruct;

import java.util.Arrays;

public class Float32 extends AbstractMatrix<float[]> {
	
	public static final Float32 instance = new Float32();
	
	@Override
	public void print(Matrix<float[]> source) {
		for(int row = 0 ; row < source.numRows() ; row++) {
  			for(int col = 0 ; col < source.numColumns() ; col++) {
  				System.out.printf("[%e] ", source.getData()[getIndex(row, col, source.numRows())]);
  			}    		
  			System.out.println();
  		}   	
	}

	@Override
	public Matrix<float[]> one(Matrix<float[]> A) {
		fill(A, 1);
		return A;
	}

	@Override
	public Matrix<float[]> zero(Matrix<float[]> A) {
		fill(A, 0);
		return A;
	}
	
	@Override
	public Matrix<float[]> fill(Matrix<float[]> source, Number value) {
		Arrays.fill(source.getData(), value.floatValue());
		return source;
	}	
	
	@Override 
	public Matrix<float[]> copy(Matrix<float[]> source){
		Matrix<float[]> matrix = new Matrix<float[]>(source.numRows(), source.numColumns(), source.getData(), true);
		return matrix;
	}

	@Override
	public float[] create(int numRows, int numColumns) {
		return new float[numRows*numColumns];
	}
	
	@Override
	public void head(Matrix<float[]> source, int numRows, int numColumns, String format) {
		numRows = numRows       > source.numRows()    ? source.numRows()    : numRows;
		numColumns = numColumns > source.numColumns() ? source.numColumns() : numColumns;	
		for(int row = 0 ; row < numRows ; row++) { 
			for(int col= 0 ; col < numColumns ; col++)
				System.out.format(format, source.getData()[getIndex(row, col, numRows)]);
			System.out.println();			
		}	
	}
	
	@Override
	public String toString(Matrix<float[]> source, String separator) {		
		String out = "";					
		for(int row = 0 ; row < source.numRows() ; row++)
			for(int col = 0 ; col < source.numColumns() ; col++)
				out += source.getData()[getIndex(row, col, source.numRows())] + separator;						
		return out.substring(0, out.length()-1);		
	}

	@Override
	public Matrix<float[]> row(Matrix<float[]> A, int row) {
		Matrix<float[]> C = new Matrix<>(float[].class, 1, A.numColumns());		
		return row(A, row, C);
	}

	@Override
	public Matrix<float[]> row(Matrix<float[]> A, int row, Matrix<float[]> C) {
		for (int col = 0; col < A.numColumns(); col++)
			C.getData()[getIndex(0, col, 1)] = A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	public Matrix<float[]> column(Matrix<float[]> A, int column) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), 1);		
		return row(A, column, C);
	}

	@Override
	public Matrix<float[]> column(Matrix<float[]> A, int column, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++)
			C.getData()[getIndex(row, 0, 1)] = A.getData()[getIndex(row, column, A.numRows())];
		return C;
	}

	@Override
	public Matrix<float[]> biggerthen(Matrix<float[]> A, Number t) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());		
		return biggerthen(A, t, C);
	}

	@Override
	public Matrix<float[]> biggerthen(Matrix<float[]> A, Number t, Matrix<float[]> C) {
		float rt = t.floatValue();		
		for (int row = 0; row < A.numRows(); row++) 
	        for (int col = 0; col < A.numColumns(); col++)
	        	if(A.getData()[getIndex(row, col, A.numRows())] > rt)
	        		C.getData()[getIndex(row, col, C.numRows())] = 1;
	        	else
	        		C.getData()[getIndex(row, col, C.numRows())] = 0;
	        		
		return C;
	}

	@Override
	public Matrix<float[]> lessthen(Matrix<float[]> A, Number t) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());	
		return lessthen(A, t, C);
	}

	@Override
	public Matrix<float[]> lessthen(Matrix<float[]> A, Number t, Matrix<float[]> C) {
		float rt = t.floatValue();		
		for (int row = 0; row < A.numRows(); row++) 
	        for (int col = 0; col < A.numColumns(); col++)
	        	if(A.getData()[getIndex(row, col, A.numRows())] < rt)
	        		C.getData()[getIndex(row, col, C.numRows())] = 1;
	        	else
	        		C.getData()[getIndex(row, col, C.numRows())] = 0;
	        		
		return C;
	}

	@Override
	public Matrix<float[]> threshold(Matrix<float[]> A, Number t) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());	
		return threshold(A, t, C);
	}

	@Override
	public Matrix<float[]> threshold(Matrix<float[]> A, Number t, Matrix<float[]> C) {
		return biggerthen(A, t, C);
	}
	
	@Override
	public Matrix<float[]> sum(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, 1, 1); 
		return sum(A, 1, 1, C);
	}

	@Override
	public Matrix<float[]> sum(Matrix<float[]> A, int axis) {
		Matrix<float[]> C;
    	if(axis == 0) {    		
    		C = new Matrix<>(float[].class, 1, A.numColumns());
    		return sum(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){    
    		C = new Matrix<>(float[].class, A.numRows(), 1);
    		return sum(A, A.numRows(), 1, C);    		
    	}else {			
			 throw new IllegalArgumentException("Invalid axis");			
		}		    	    	
	}

	public Matrix<float[]> sum(Matrix<float[]> A, int r, int c, Matrix<float[]> C){		
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)             	            	
            	C.getData()[getIndex(row%r, col%c, C.numRows())] = C.getData()[getIndex(row%r, col%c, C.numRows())] + A.getData()[getIndex(row, col, A.numRows())];            	         
		return C;    
	}

	@Override
	public Matrix<float[]> sumn(Matrix<float[]> A) {
		Matrix<float[]> aux1 = new Matrix<>(float[].class, 1, A.numRows());
		aux1.one();
		Matrix<float[]> aux2 = new Matrix<>(float[].class, 1, A.numColumns());
		Matrix<float[]> aux3 = new Matrix<>(float[].class, A.numColumns(), 1);
		aux3.one();
		Matrix<float[]> C = new Matrix<>(float[].class, 1, 1);			
		Algebra.multAdd(1, aux1, A, aux2);
		Algebra.multAdd(1, aux2, aux3, C);						
		return C;		
	}

	@Override
	public Matrix<float[]> sumn(Matrix<float[]> A, int axis) {
		Matrix<float[]> B;
		Matrix<float[]> C;		
		if(axis == 0) {
			B = new Matrix<>(float[].class, 1, A.numRows());
			B.one();
			C = new Matrix<>(float[].class, 1, A.numColumns());
			Algebra.multAdd(1, B, A, C);
			return C;
		} else if(axis == 1) {
			B = new Matrix<>(float[].class, A.numColumns(), 1);
			B.one();
			C = new Matrix<>(float[].class, A.numRows(), 1);
			Algebra.multAdd(1, A, B, C);
			return C;
		} else {			
			throw new IllegalArgumentException("Invalid axis");			
		}
	}

	@Override
	public Matrix<float[]> sumn(Matrix<float[]> A, int axis, Matrix<float[]> C) {
		Matrix<float[]> B;
		if(axis == 0) {
			B = new Matrix<>(float[].class, 1, A.numRows());
			B.one();
			Algebra.multAdd(1, B, A, C);
			return C;
		} else if(axis == 1) {
			B = new Matrix<>(float[].class, A.numColumns(), 1);
			B.one();
			Algebra.multAdd(1, A, B, C);
			return C;
		} else {			
			throw new IllegalArgumentException("Invalid axis");			
		}		
	}

	@Override
	public Matrix<float[]> sumn(Matrix<float[]> A, int axis, Matrix<float[]> B, Matrix<float[]> C) {
		if(axis == 0) {
			Algebra.multAdd(1, B, A, C);
			return C;
		} else if(axis == 1) {		
			Algebra.multAdd(1, A, B, C);
			return C;
		} else {			
			throw new IllegalArgumentException("Invalid axis");			
		}		
	}

	@Override
	public Matrix<float[]> T(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numColumns(), A.numRows());
		return T(A, C);
	}

	@Override
	public Matrix<float[]> T(Matrix<float[]> A, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	
            	C.getData()[getIndex(col, row, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())]; 		
		return C;		
	}

	@Override
	public Matrix<float[]> muli(Matrix<float[]> A, Matrix<float[]> B) {
		if(B.isScalar()) {
			return mul(A, B.getData()[0], A);
		}		
		return mul(A, B, A);
	}

	@Override
	public Matrix<float[]> mul(Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {	
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] * B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];  							
		return C;       
	}

	@Override
	public Matrix<float[]> mul(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return mul(A, B.getData()[0], C);
		}		
		return mul(A, B, C);
	}

	@Override
	public Matrix<float[]> mul(Matrix<float[]> A, Number value) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());   			       
		return mul(A, value, C); 
	}

	@Override
	public Matrix<float[]> mul(Matrix<float[]> A, Number value, Matrix<float[]> C) {
		float val = value.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] * val;
		return C;       
	}

	@Override
	public Matrix<float[]> plusni(Matrix<float[]> A, Matrix<float[]> B) {
		Algebra.fplus(A.numRows()*A.numColumns(), A,  B);
		return A;
	}

	@Override
	public Matrix<float[]> plusn(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = A.copy();
		Algebra.fplus(A.numRows()*A.numColumns(), C,  B);
		return C;
	}

	@Override
	public Matrix<float[]> plusi(Matrix<float[]> A, Matrix<float[]> B) {
		if(B.isScalar()) {
			return plus(A, B.getData()[0], A);
		}    		
		return plus(A, B, A);
	}

	@Override
	public Matrix<float[]> plus(Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] + B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];		
		return C;
	}

	@Override
	public Matrix<float[]> plus(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return plus(A, B.getData()[0], C);
		}    		
		return plus(A, B, C);
	}

	@Override
	public Matrix<float[]> plus(Matrix<float[]> A, Number value) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());	   
		return plus(A, value, C);			
	}

	@Override
	public Matrix<float[]> plus(Matrix<float[]> A, Number value, Matrix<float[]> C) {
		float val = value.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] + val;
		return C;
	}

	@Override
	public Matrix<float[]> minusni(Matrix<float[]> A, Matrix<float[]> B) {
		Algebra.fminus(A.numRows()*A.numColumns(), A, B);
		return A;
	}

	@Override
	public Matrix<float[]> minusn(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = A.copy();
		Algebra.fminus(A.numRows()*A.numColumns(), C, B);
		return C;
	}

	@Override
	public Matrix<float[]> minusi(Matrix<float[]> A, Matrix<float[]> B) {
		if(B.isScalar()) {
			return minus(A, B.getData()[0], A);
		}		
        return minus(A, B, A);
	}

	@Override
	public Matrix<float[]> minus(Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] - B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];		
		return C;
	}

	@Override
	public Matrix<float[]> minus(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return minus(A, B.getData()[0], C);
		}    		
		return minus(A, B, C);
	}

	@Override
	public Matrix<float[]> minus(Matrix<float[]> A, Number value) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());	   
		return minus(A, value, C);			
	}

	@Override
	public Matrix<float[]> minus(Matrix<float[]> A, Number value, Matrix<float[]> C) {
		float val = value.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] - val;
		return C;
	}

	@Override
	public Matrix<float[]> rdiv(Matrix<float[]> A, Number value) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());	   
		return rdiv(A, value, C);			
	}

	@Override
	public Matrix<float[]> rdiv(Matrix<float[]> A, Number value, Matrix<float[]> C) {
		float val = value.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = val / A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	public Matrix<float[]> divi(Matrix<float[]> A, Matrix<float[]> B) {
		if(B.isScalar()) {
			return div(A, B.getData()[0], A);
		}    		
		return div(A, B, A);
	}

	@Override
	public Matrix<float[]> div(Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] / B.getData()[getIndex(row%B.numRows(), col%B.numColumns(), B.numRows())];		
		return C;
	}

	@Override
	public Matrix<float[]> div(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		if(B.isScalar()) {
			return div(A, B.getData()[0], C);
		}    		
		return div(A, B, C);
	}

	@Override
	public Matrix<float[]> div(Matrix<float[]> A, Number value) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());	   
		return div(A, value, C);			
	}

	@Override
	public Matrix<float[]> div(Matrix<float[]> A, Number value, Matrix<float[]> C) {
		float val = value.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] / val;
		return C;
	}

	@Override
	public Matrix<float[]> dot(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), B.numColumns());
		Algebra.multAdd(1, A, B, C);
		return C;
	}

	@Override
	public Matrix<float[]> dot(Matrix<float[]> A, Number value, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), B.numColumns());
		return Algebra.multAdd(value.floatValue(), A, B, C);
	}

	@Override
	public Matrix<float[]> dot(Matrix<float[]> A, Number value, Matrix<float[]> B, Matrix<float[]> C) {
		return Algebra.multAdd(value.floatValue(), A, B, C);
	}
	
	
	
	@Override
	public Matrix<float[]> Tdot(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numColumns(), B.numColumns());
		return Tdot(A, B, C);
	}

	@Override
	Matrix<float[]> Tdot(Matrix<float[]> A, Number value, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numColumns(), B.numColumns());		
		Algebra.transAmultAdd(value.floatValue(), A, B, C);
		return C;
	}

	@Override
	Matrix<float[]> Tdot(Matrix<float[]> A, Number value, Matrix<float[]> B, Matrix<float[]> C) {
		Algebra.transAmultAdd(value.floatValue(), A, B, C);
		return C;
	}

	@Override
	Matrix<float[]> dotT(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), B.numRows());
		Algebra.transBmultAdd(1, A, B, C);
		return C;
	}

	@Override
	public Matrix<float[]> dotT(Matrix<float[]> A, Number value, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), B.numRows());
		Algebra.transBmultAdd(value.floatValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<float[]> dotT(Matrix<float[]> A, Number value, Matrix<float[]> B, Matrix<float[]> C) {
		Algebra.transBmultAdd(value.floatValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<float[]> TdotT(Matrix<float[]> A, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numColumns(), B.numRows());		
		return TdotT(A, B, C);
	}

	@Override
	public Matrix<float[]> TdotT(Matrix<float[]> A, Number value, Matrix<float[]> B) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numColumns(), B.numRows());
		Algebra.transABmultAdd(value.floatValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<float[]> TdotT(Matrix<float[]> A, Number value, Matrix<float[]> B, Matrix<float[]> C) {
		Algebra.transABmultAdd(value.floatValue(), A, B, C);
		return C;
	}

	@Override
	public Matrix<float[]> neg(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numColumns(), A.numRows());
		return neg(C);
	}

	@Override
	public Matrix<float[]> neg(Matrix<float[]> A, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = -A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	Matrix<float[]> mean(Matrix<float[]> A, int r, int c, Number div, Matrix<float[]> C) {
		sum(A, r, c, C);
		return divi(C, div);
	}
	
	@Override
	public Matrix<float[]> mean(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, 1, 1);
		sum(A, 1, 1, C);
		return C;
	}

	@Override
	public Matrix<float[]> mean(Matrix<float[]> A, int axis, Matrix<float[]> C) {
		if(axis == 0) {
			return mean(A, 1, A.numColumns(), A.numRows(), C);    		
		}else if(axis == 1){
			return mean(A, A.numRows(), 1, A.numColumns(), C);    	
		} else {
			throw new IllegalArgumentException("Invalid axis");
		}    	  
	}

	@Override
	public Matrix<float[]> mean(Matrix<float[]> A, int axis) {
		Matrix<float[]>  C;		
		if(axis == 0) {
			C = new  Matrix<>(float[].class,1, A.numColumns());    		
			return mean(A, 1, A.numColumns(), A.numRows(), C);    		
		}else if(axis == 1){
			C = new  Matrix<>(float[].class, A.numRows(), 1);
			return mean(A, A.numRows(), 1, A.numColumns(), C);    	
		} else {
			throw new IllegalArgumentException("Invalid axis");
		}    	   
	}

	@Override
	public Matrix<float[]> meann(Matrix<float[]> A) {
		Matrix<float[]> C = sumn(A);
		C.getData()[0] = C.getData()[0] / (A.numRows()*A.numColumns());		
		return C;
	}
	
	@Override
	public Matrix<float[]> meann(Matrix<float[]> A, int axis){
		Matrix<float[]> C = sumn(A, axis);
		if(axis == 0)
			C.divi(A.numRows());
		else
			C.divi(A.numColumns());
		return C;
	}

	@Override
	public Matrix<float[]> pow2(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		return pow2(A, C);
	}

	@Override
	public Matrix<float[]> pow2(Matrix<float[]> A, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = A.getData()[getIndex(row, col, A.numRows())] * A.getData()[getIndex(row, col, A.numRows())];
		return C;
	}

	@Override
	public Matrix<float[]> pow(Matrix<float[]> A, Number exponent) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		return pow(A, exponent,  C);
	}

	@Override
	public Matrix<float[]> pow(Matrix<float[]> A, Number exponent, Matrix<float[]> C) {
		float exp = exponent.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = (float) Math.pow(A.getData()[getIndex(row, col, A.numRows())], exp) ;
		return C;
	}

	@Override
	public Matrix<float[]> sqrt(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		return sqrt(A, C);
	}

	@Override
	public Matrix<float[]> sqrt(Matrix<float[]> A, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = (float) Math.sqrt(A.getData()[getIndex(row, col, A.numRows())]);
		return C;
	}

	@Override
	public Matrix<float[]> exp(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		return exp(A, C);
	}

	@Override
	public Matrix<float[]> exp(Matrix<float[]> A, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = (float) Math.exp(A.getData()[getIndex(row, col, A.numRows())]);
		return C;
	}

	@Override
	public Matrix<float[]> tanh(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		return tanh(A, C);
	}

	@Override
	public Matrix<float[]> tanh(Matrix<float[]> A, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
			for (int col = 0; col < A.numColumns(); col++)				
				C.getData()[getIndex(row, col, C.numRows())] = (float) Math.tanh(A.getData()[getIndex(row, col, A.numRows())]);
		return C;
	} 
	
	@Override
	public Matrix<float[]> var(Matrix<float[]> A, int axis, int d) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		if(axis == 0) {    		
    		return var(A, axis, A.numRows(), d, C);    		
    	}else if(axis == 1){    		
    		return var(A, axis, A.numColumns(), d, C);    		
    	}else {
			throw new IllegalArgumentException("Invalid axis");
		}    	      	
	}

	@Override
	public Matrix<float[]> varn(Matrix<float[]> A, int axis, int d) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns()); 
		if(axis == 0) {
    		return varn(A, axis, A.numRows(), d, C);    		
    	}else if(axis == 1){
    		return varn(A, axis, A.numColumns(), d, C);    	
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}    			
	}

	@Override
	public Matrix<float[]> std(Matrix<float[]> A, int axis, int d) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		return std(A, axis, d, C);
	}

	@Override
	public Matrix<float[]> stdn(Matrix<float[]> A, int axis, int d) {
		Matrix<float[]> C = new Matrix<>(float[].class, A.numRows(), A.numColumns());
		if(axis == 0) {
    		return stdn(A, axis, A.numRows(), d, C);    		
    	}else if(axis == 1){
    		return stdn(A, axis, A.numColumns(), d, C);    	
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}    	 
	}
	
	Matrix<float[]> max(Matrix<float[]> A, int r, int c, Matrix<float[]> C){
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)
            	C.getData()[getIndex(row%r, col%c, C.numRows())] =  Math.max(C.getData()[getIndex(row%r, col%c, C.numRows())], A.getData()[getIndex(row, col, A.numRows())]);                      
		return C;    	
	}

	@Override
	public Matrix<float[]> max(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, 1, 1);
		C.fill(Double.MIN_VALUE);
		return max(A, 1, 1, C);
	}

	@Override
	public Matrix<float[]> max(Matrix<float[]> A, int axis) {
		Matrix<float[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(float[].class, 1, A.numColumns());
    		C.fill(Double.MIN_VALUE);
    		return max(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(float[].class, A.numRows(), 1);
    		C.fill(Double.MIN_VALUE);
    		return max(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}       	
	}

	@Override
	public Matrix<float[]> max(Matrix<float[]> A, Number value, Matrix<float[]> C) {
		float val = value.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	
            	C.getData()[getIndex(row, col, C.numRows())] = Math.max(val, A.getData()[getIndex(row, col, A.numRows())]);            	         
		return C;
	}
	
	Matrix<float[]> min(Matrix<float[]> A, int r, int c, Matrix<float[]> C){
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)
            	C.getData()[getIndex(row%r, col%c, C.numRows())] =  Math.min(C.getData()[getIndex(row%r, col%c, C.numRows())], A.getData()[getIndex(row, col, A.numRows())]);                      
		return C;    	
	}

	@Override
	public Matrix<float[]> min(Matrix<float[]> A) {
		Matrix<float[]> C = new Matrix<>(float[].class, 1, 1);
		C.fill(Double.MAX_VALUE);
		return min(A, 1, 1, C);
	}

	@Override
	public Matrix<float[]> min(Matrix<float[]> A, int axis) {
		Matrix<float[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(float[].class, 1, A.numColumns());
    		C.fill(Double.MAX_VALUE);
    		return min(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(float[].class, A.numRows(), 1);
    		C.fill(Double.MAX_VALUE);
    		return min(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}       
	}

	@Override
	public Matrix<float[]> min(Matrix<float[]> A, Number value, Matrix<float[]> C) {
		float val = value.floatValue();
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	
            	C.getData()[getIndex(row, col, C.numRows())] = Math.min(val, A.getData()[getIndex(row, col, A.numRows())]);            	         
		return C;
	}

	@Override
	public Matrix<float[]> argmax(Matrix<float[]> A, int axis) {
		Matrix<float[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(float[].class, 1, A.numColumns());
    		return rowargmax(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(float[].class, A.numRows(), 1);
    		return colargmax(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}     
	}
	
	Matrix<float[]> colargmax(Matrix<float[]> A, int r, int c, Matrix<float[]> C) {		
		Matrix<float[]> aux = new Matrix<>(float[].class, r, c);		
		aux.fill(Double.MIN_VALUE);		
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] > aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = col;            		            		
            	}    	    	
		return C;		
    }
	
	Matrix<float[]> rowargmax(Matrix<float[]>A, int r, int c, Matrix<float[]> C) {
		Matrix<float[]> aux = new Matrix<>(float[].class, r, c);		
		aux.fill(Double.MIN_VALUE);
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] > aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = row;            		            		
            	}    	    	
		return C;		   	
    }

	@Override
	public Matrix<float[]> argmin(Matrix<float[]> A, int axis) {
		Matrix<float[]> C;
    	if(axis == 0) {
    		C = new Matrix<>(float[].class, 1, A.numColumns());
    		return rowargmin(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){
    		C = new Matrix<>(float[].class, A.numRows(), 1);
    		return colargmin(A, A.numRows(), 1, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}     
	}

	Matrix<float[]> colargmin(Matrix<float[]> A, int r, int c, Matrix<float[]> C) {		
		Matrix<float[]> aux = new Matrix<>(float[].class, r, c);		
		aux.fill(Double.MAX_VALUE);		
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] < aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = col;            		            		
            	}    	    	
		return C;		
    }
	
	Matrix<float[]> rowargmin(Matrix<float[]>A, int r, int c, Matrix<float[]> C) {
		Matrix<float[]> aux = new Matrix<>(float[].class, r, c);		
		aux.fill(Double.MAX_VALUE);
    	for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)            	            
            	if( A.getData()[getIndex(row, col, A.numRows())] < aux.getData()[getIndex(row%r, col%c, aux.numRows())] ) {            		
            		aux.getData()[getIndex(row%r, col%c, aux.numRows())] = A.getData()[getIndex(row, col, A.numRows())];            	    
            		C.getData()[getIndex(row%r, col%c, C.numRows())] = row;            		            		
            	}    	    	
		return C;		   	
    }
	
	@Override
	public Matrix<float[]> sigmoid(Matrix<float[]> A, Matrix<float[]> C) {
		for (int row = 0; row < A.numRows(); row++) 
            for (int col = 0; col < A.numColumns(); col++)             	
            	C.getData()[getIndex(row, col, C.numRows())] = (float) (1.0/(1.0 + Math.exp(-A.getData()[getIndex(row, col, A.numRows())])));            			
		return C;
	}

	@Override
	public float[] clone(float[] A) {
		return A.clone();
	}

}
