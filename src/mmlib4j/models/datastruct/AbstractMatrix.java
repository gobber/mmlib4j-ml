package mmlib4j.models.datastruct;

public abstract class AbstractMatrix<N> {	
	
	abstract public N create(int numRows, int numColumns);
	
	abstract public void print(Matrix<N> A);
	
	abstract public Matrix<N> fill(Matrix<N> A, Number value);
	
	abstract public Matrix<N> copy(Matrix<N> A);
	
	abstract public N clone(N A);
	
	abstract public void head(Matrix<N> A, int numRows, int numColumns, String format);
	
	public void head(Matrix<N> A, int numRows, int numColumns) {
		head(A, numRows, numColumns, "%e");
	}
	
	public void head(Matrix<N> A) {
		head(A, 6, 7);
	}
	
	abstract public Matrix<N> one(Matrix<N> A);
	
	abstract public Matrix<N> zero(Matrix<N> A);
	
	int getIndex(int row, int col, int numRows) {
		return row + col * numRows;
	}
	
	abstract public String toString(Matrix<N> A, String separator);
	
	abstract public Matrix<N> row(Matrix<N> A, int row);
	
	abstract public Matrix<N> row(Matrix<N> A, int row, Matrix<N> C);
	
	abstract public Matrix<N> column(Matrix<N> A, int column);
	
	abstract public Matrix<N> column(Matrix<N> A, int column, Matrix<N> C);
	
	abstract public Matrix<N> biggerthen(Matrix<N> A, Number t);

	abstract public Matrix<N> biggerthen(Matrix<N> A, Number t, Matrix<N> C);
	
	public Matrix<N> biggertheni(Matrix<N> A, Number t){
		return biggerthen(A, t, A);
	}
	
	abstract public Matrix<N> lessthen(Matrix<N> A, Number t);
	
	abstract public Matrix<N> lessthen(Matrix<N> A, Number t, Matrix<N> C);
	
	public Matrix<N> lesstheni(Matrix<N> A, Number t){
		return lessthen(A, t, A);
	}
	
	abstract public Matrix<N> threshold(Matrix<N> A, Number t);
	
	abstract public Matrix<N> threshold(Matrix<N> A, Number t, Matrix<N> C);
	
	public Matrix<N> thresholdi(Matrix<N> A, Number t){
		return threshold(A, t, A);
	}
	
	abstract public Matrix<N> sum(Matrix<N> A);
	
	abstract public Matrix<N> sum(Matrix<N> A, int axis);
	
	abstract public Matrix<N> sum(Matrix<N> A, int r, int c, Matrix<N> C);
	
	public Matrix<N> sum(Matrix<N> A, int axis, Matrix<N> C){
		if(axis == 0) {    		
    		return sum(A, 1, A.numColumns(), C);    		
    	}else if(axis == 1){    		
    		return sum(A, A.numRows(), 1, C);    		
    	}else {			
			 throw new IllegalArgumentException("Invalid axis");			
		}    
	}
	
	abstract public Matrix<N> sumn(Matrix<N> A);
	
	abstract public Matrix<N> sumn(Matrix<N> A, int axis);
	
	abstract public Matrix<N> sumn(Matrix<N> A, int axis, Matrix<N> C);
	
	abstract public Matrix<N> sumn(Matrix<N> A, int axis,  Matrix<N> B, Matrix<N> C);	
	
	abstract public Matrix<N> T(Matrix<N> A);
	
	abstract public Matrix<N> T(Matrix<N> A, Matrix<N> C);
	
	abstract public Matrix<N> muli(Matrix<N> A, Matrix<N> B);
	
	public Matrix<N> muli(Matrix<N> A, Number value){
		return mul(A, value, A);
	}
	
	abstract public Matrix<N> mul(Matrix<N> A, Matrix<N> B, Matrix<N> C);
	
	abstract public Matrix<N> mul(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> mul(Matrix<N> A, Number value);
	
	abstract public Matrix<N> mul(Matrix<N> A, Number value, Matrix<N> C);
	
	abstract public Matrix<N> plusni(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> plusn(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> plusi(Matrix<N> A, Matrix<N> B);
	
	public Matrix<N> plusi(Matrix<N> A, Number value){
		return plus(A, value, A);
	}
	
	abstract public Matrix<N> plus(Matrix<N> A, Matrix<N> B, Matrix<N> C);
	
	abstract public Matrix<N> plus(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> plus(Matrix<N> A, Number value);
	
	abstract public Matrix<N> plus(Matrix<N> A, Number value, Matrix<N> C);
	
	abstract public Matrix<N> minusni(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> minusn(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> minusi(Matrix<N> A, Matrix<N> B);
	
	public Matrix<N> minusi(Matrix<N> A, Number value){
		return minus(A, value, A);
	}
	
	abstract public Matrix<N> minus(Matrix<N> A, Matrix<N> B, Matrix<N> C);
	
	abstract public Matrix<N> minus(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> minus(Matrix<N> A, Number value);
	
	abstract public Matrix<N> minus(Matrix<N> A, Number value, Matrix<N> C);
	
	public Matrix<N> rdivi(Matrix<N> A, Number value){
		return rdiv(A, value, A);
	}
	
	abstract public Matrix<N> rdiv(Matrix<N> A, Number value);
	
	abstract public Matrix<N> rdiv(Matrix<N> A, Number value, Matrix<N> C);
	
	public Matrix<N> divi(Matrix<N> A, Number value){
		return div(A, value, A);
	}
	
	abstract public Matrix<N> divi(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> div(Matrix<N> A, Matrix<N> B, Matrix<N> C);
	
	abstract public Matrix<N> div(Matrix<N> A, Matrix<N> B);
	
	abstract public Matrix<N> div(Matrix<N> A, Number value);
	
	abstract public Matrix<N> div(Matrix<N> A, Number value, Matrix<N> C);
	
	abstract public Matrix<N> dot(Matrix<N> A, Matrix<N> B);
	
	public Matrix<N> dot(Matrix<N> A, Matrix<N> B, Matrix<N> C){
		return dot(A, 1, B, C);
	}
	
	abstract public Matrix<N> dot(Matrix<N> A, Number value, Matrix<N> B);
	
	abstract public Matrix<N> dot(Matrix<N> A, Number value, Matrix<N> B, Matrix<N> C);
	
	abstract public Matrix<N> Tdot(Matrix<N> A, Matrix<N> B);
	
	public Matrix<N> Tdot(Matrix<N> A, Matrix<N> B, Matrix<N> C){
		return Tdot(A, 1, B, C);
	}
	
	abstract Matrix<N> Tdot(Matrix<N> A, Number value, Matrix<N> B);
	
	abstract Matrix<N> Tdot(Matrix<N> A, Number value, Matrix<N> B, Matrix<N> C);
	
	abstract Matrix<N> dotT(Matrix<N> A, Matrix<N> B);
	
	public Matrix<N> dotT(Matrix<N> A, Matrix<N> B, Matrix<N> C){
		return dotT(A, 1, B, C);
	}
	
	abstract public Matrix<N> dotT(Matrix<N> A, Number value, Matrix<N> B);
	
	abstract public Matrix<N> dotT(Matrix<N> A, Number value, Matrix<N> B, Matrix<N> C);
	
	abstract public Matrix<N> TdotT(Matrix<N> A, Matrix<N> B);
	
	public Matrix<N> TdotT(Matrix<N> A, Matrix<N> B, Matrix<N> C){
		return TdotT(A, 1, B, C);
	}
	
	abstract public Matrix<N> TdotT(Matrix<N> A, Number value, Matrix<N> B);
	
	abstract public Matrix<N> TdotT(Matrix<N> A, Number value, Matrix<N> B, Matrix<N> C);
	
	public Matrix<N> negi(Matrix<N> A){
		return neg(A, A);
	}
	
	abstract public Matrix<N> neg(Matrix<N> A);
	
	abstract public Matrix<N> neg(Matrix<N> A, Matrix<N> C);
	
	abstract public Matrix<N> mean(Matrix<N> A);
	
	abstract public Matrix<N> mean(Matrix<N> A, int axis, Matrix<N> C);
	
	abstract public Matrix<N> mean(Matrix<N> A, int axis);
	
	abstract Matrix<N> mean(Matrix<N> A, int r, int c, Number div, Matrix<N> C);
	
	abstract public Matrix<N> meann(Matrix<N> A);
	
	abstract public Matrix<N> meann(Matrix<N> A, int axis);
	
	public Matrix<N> pow2i(Matrix<N> A){
		return pow2(A, A);
	}
	
	abstract public Matrix<N> pow2(Matrix<N> A);
	
	abstract public Matrix<N> pow2(Matrix<N> A, Matrix<N> C);
	
	public Matrix<N> powi(Matrix<N> A, Number exponent){
		return pow(A, exponent, A);
	}
	
	abstract public Matrix<N> pow(Matrix<N> A, Number exponent);
	
	abstract public Matrix<N> pow(Matrix<N> A, Number exponent, Matrix<N> C);
	
	public Matrix<N> sqrti(Matrix<N> A){
		return sqrt(A, A);
	}
	
	abstract public Matrix<N> sqrt(Matrix<N> A);
	
	abstract public Matrix<N> sqrt(Matrix<N> A, Matrix<N> C);
	
	public Matrix<N> expi(Matrix<N> A){
		return exp(A, A);
	}
	
	abstract public Matrix<N> exp(Matrix<N> A);
	
	abstract public Matrix<N> exp(Matrix<N> A, Matrix<N> C);
	
	public Matrix<N> tanhi(Matrix<N> A){
		return exp(A, A);
	}
	
	abstract public Matrix<N> tanh(Matrix<N> A);
	
	abstract public Matrix<N> tanh(Matrix<N> A, Matrix<N> C);	
	
	abstract public Matrix<N> var(Matrix<N> A, int axis, int d);
	
	abstract public Matrix<N> varn(Matrix<N> A, int axis, int d);
	
	public Matrix<N> var(Matrix<N> A, int axis, int div, int d, Matrix<N> C) {		
		Matrix<N> mean = A.mean(axis);
		Matrix<N> aux  = A.minus(mean, C).pow2i();		
    	mean.zero();    	    	
		return aux.sum(axis, mean).divi(div-d);		    	
    }
	
	public Matrix<N> varn(Matrix<N> A, int axis, int div, int d, Matrix<N> C) {		
		Matrix<N> mean = A.meann(axis);
		Matrix<N> aux  = A.minus(mean, C).powi(2);
		mean.zero();		
		return aux.sumn(axis, mean).divi(div-d);
	}
	
	public Matrix<N> std(Matrix<N> A, int axis, int div, int d, Matrix<N> C) {		
		Matrix<N> var = var(A, axis, div, d, C);
		return sqrti(var);					
    }
	
	public Matrix<N> std(Matrix<N> A, int axis, int d, Matrix<N> C){
		if(axis == 0) {    		
    		return std(A, axis, A.numRows(), d, C);    		
    	}else if(axis == 1){    		
    		return std(A, axis, A.numColumns(), d, C);    		
    	} else {
    		throw new IllegalArgumentException("Invalid axis");
    	}       	
	}
	
	public Matrix<N> stdn(Matrix<N> A, int axis, int div, int d, Matrix<N> C) {				
		Matrix<N> var = varn(A, axis, div, d, C);	
		return sqrti(var);				
	} 
	
	abstract public Matrix<N> std(Matrix<N> A, int axis, int d);
	
	abstract public Matrix<N> stdn(Matrix<N> A, int axis, int d);
	
	abstract public Matrix<N> max(Matrix<N> A);
	
	abstract public Matrix<N> max(Matrix<N> A, int axis);
	
	abstract public Matrix<N> max(Matrix<N> A, Number value, Matrix<N> C);
	
	public Matrix<N> maxi(Matrix<N> A, Number value){
		return max(A, value, A);
	}
	
	abstract public Matrix<N> min(Matrix<N> A);
	
	abstract public Matrix<N> min(Matrix<N> A, int axis);
	
	abstract public Matrix<N> min(Matrix<N> A, Number value, Matrix<N> C);
	
	abstract public Matrix<N> argmax(Matrix<N> A, int axis);
	
	abstract public Matrix<N> argmin(Matrix<N> A, int axis);
	
	abstract public Matrix<N> sigmoid(Matrix<N> A, Matrix<N> C);
	
	public Matrix<N> sigmoidi(Matrix<N> A){
		return sigmoid(A, A);
	}
	
}

