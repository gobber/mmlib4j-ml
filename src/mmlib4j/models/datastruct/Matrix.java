package mmlib4j.models.datastruct;

import mmlib4j.utils.Random;

public class Matrix<N> {

	private N data;
	private int numRows;
	private int numColumns;
	private AbstractMatrix<N> matrix;
	private String separator = ";";
	
	@SuppressWarnings("unchecked")
	public Matrix (Class<N> arrayType, int numRows, int numColumns)	{		
		switch (arrayType.getCanonicalName()) {
		case "double[]":
			this.matrix = (AbstractMatrix<N>) Float64.instance;
			break;

		case "float[]":
			this.matrix = (AbstractMatrix<N>) Float32.instance;
			break;
		default:
			throw new IllegalArgumentException("Not allowed data type.");
		}				
		this.data = matrix.create(numRows, numColumns);			
		this.numRows = numRows;
		this.numColumns = numColumns;
	}
	
	@SuppressWarnings("unchecked")
	public Matrix (float values[], int numRows, int numColumns) {
		this.numRows = numRows;
		this.numColumns = numColumns;	
		this.matrix = (AbstractMatrix<N>) Float32.instance;
		this.data = (N) values;
	}
	
	@SuppressWarnings("unchecked")
	public Matrix (double values[], int numRows, int numColumns) {
		this.numRows = numRows;
		this.numColumns = numColumns;	
		this.matrix = (AbstractMatrix<N>) Float64.instance;
		this.data = (N) values;
	}
	
	@SuppressWarnings("unchecked")
	public Matrix (int numRows, int numColumns, double[] values, boolean deep) {
		this.numRows = numRows;
		this.numColumns = numColumns;	
		this.matrix = (AbstractMatrix<N>) Float64.instance;		
		if(deep)
			data = (N) values.clone();
		else
			data = (N) values;				
	}
	
	@SuppressWarnings("unchecked")
	public Matrix (int numRows, int numColumns, float[] values, boolean deep) {
		this.numRows = numRows;
		this.numColumns = numColumns;	
		this.matrix = (AbstractMatrix<N>) Float32.instance;		
		if(deep)
			data = (N) values.clone();
		else
			data = (N) values;
	}
	
	@SuppressWarnings("unchecked")
	public Matrix (double values[][]) {		
		this.numRows = values.length;
		this.numColumns = values[0].length;
		this.matrix = (AbstractMatrix<N>) Float64.instance;	
		double [] temp = new double[this.numRows * this.numColumns];		
		for (int row = 0; row < numRows; row++) 
		       for (int col = 0; col < numColumns; col++)
		    	   temp[row + col * numRows] = values[row][col];		
		data = (N) temp;
	}
	
	@SuppressWarnings("unchecked")
	public Matrix (float values[][]) {		
		this.numRows = values.length;
		this.numColumns = values[0].length;
		this.matrix = (AbstractMatrix<N>) Float32.instance;	
		float [] temp = new float[this.numRows * this.numColumns];		
		for (int row = 0; row < numRows; row++) 
		       for (int col = 0; col < numColumns; col++)
		    	   temp[row + col * numRows] = values[row][col];		
		data = (N) temp;
	}
	
	@SuppressWarnings("unchecked")
	public Matrix (Class<N> arrayType, int numRows, int numColumns, N values, boolean deep)	{		
		switch (arrayType.getCanonicalName()) {
		case "double[]":
			this.matrix = (AbstractMatrix<N>) Float64.instance;
			break;

		case "float[]":
			this.matrix = (AbstractMatrix<N>) Float32.instance;
			break;
		default:
			throw new IllegalArgumentException("Not allowed data type.");
		}						
		if(deep)
			data = matrix.clone(values);
		else
			data = values;						
		this.numRows = numRows;
		this.numColumns = numColumns;
	}

	
	public Matrix (int numRows, int numColumns, AbstractMatrix<N> matrix)	{		
		this.matrix = matrix;			
		this.data = matrix.create(numRows, numColumns);			
		this.numRows = numRows;
		this.numColumns = numColumns;
	}	
	
	public boolean isScalar() {
		return numRows*numColumns == 1;
	}
	
	public String shape() {
		return "("+numRows + ","+ numColumns + ")";
	}
	
	public String getSeparator() {
		return separator;
	}
	
	public void setSeparator(String separator) {
		this.separator = separator;	
	}
	
	public N getData() {
		return data;
	}

	public void setData(N data) {
		this.data = data;
	}

	public int numRows() {
		return numRows;
	}

	public void numRows(int numRows) {
		this.numRows = numRows;
	}

	public int numColumns() {
		return numColumns;
	}

	public void numColumns(int numColumns) {
		this.numColumns = numColumns;
	}
	
	public void print() {
		matrix.print(this);
	}
	
	public Matrix<N> fill(Number value){
		return matrix.fill(this, value);
	}
	
	public Matrix<N> copy(){
		return matrix.copy(this);
	}
	
	public void head(int numRows, int numColumns, String format) {
		matrix.head(this, numRows, numColumns, format);
	}
	
	public void head(int numRows, int numColumns) {
		matrix.head(this, numRows, numColumns);
	}
	
	public void head(Matrix<N> A) {
		matrix.head(A);
	}
	
	public Matrix<N> one(){
		matrix.one(this);
		return this;
	}
	
	public Matrix<N> zero(){
		matrix.zero(this);
		return this;
	}	
	
	public String toString() {
		return matrix.toString(this, separator);
	}
	
	public Matrix<N> row(int row){
		return matrix.row(this, row);
	}
	
	public Matrix<N> row(int row, Matrix<N> C){
		return matrix.row(this, row, C);
	}
	
	public Matrix<N> column(int column){
		return matrix.column(this, column);
	}
	
	public Matrix<N> column(int column, Matrix<N> C){
		return matrix.column(this, column, C);
	}
	
	public Matrix<N> biggerthen(Number t) {
		return matrix.biggerthen(this, t);
	}
	
	public Matrix<N> biggerthen(Number t, Matrix<N> C){
		return matrix.biggerthen(this, t, C);
	}
	
	public Matrix<N> biggertheni(Number t){
		return matrix.biggerthen(this, t, this);
	}
	
	public Matrix<N> lessthen(Number t){
		return matrix.lessthen(this, t, this);
	}
	
	public Matrix<N> lessthen(Number t, Matrix<N> C){
		return matrix.lessthen(this, t, C);
	}
	
	public Matrix<N> lesstheni(Number t){
		return matrix.lessthen(this, t, this);
	}
	
	public Matrix<N> threshold(Number t){
		return matrix.threshold(this, t);
	}
	
	public Matrix<N> threshold(Number t, Matrix<N> C){
		return matrix.threshold(this, t, C);
	}
	
	public Matrix<N> thresholdi(Number t){
		return matrix.threshold(this, t, this);
	}
	
	public Matrix<N> sum(){
		return matrix.sum(this);
	}
	
	public Matrix<N> sum(int axis){
		return matrix.sum(this, axis);
	}

	public Matrix<N> sum(int axis, Matrix<N> C){
		return matrix.sum(this, axis,  C);
	}
	
	public Matrix<N> sumn(){
		return matrix.sumn(this);
	}
	
	public Matrix<N> sumn(int axis){
		return matrix.sumn(this, axis);
	}
	
	public Matrix<N> sumn(int axis, Matrix<N> C){
		return matrix.sumn(this, axis, C);
	}
	
	public Matrix<N> sumn(int axis, Matrix<N> B, Matrix<N> C){
		return matrix.sumn(this, axis, B, C);
	}
	
	public Matrix<N> T() {
		return matrix.T(this);
	}
	
	public Matrix<N> T(Matrix<N> C){
		return matrix.T(this, C);
	}
	
	public Matrix<N> muli(Matrix<N> B){
		return matrix.muli(this, B);
	}
	
	public Matrix<N> muli(Number value){
		return matrix.muli(this, value);
	}

	public Matrix<N> mul(Matrix<N> B, Matrix<N> C){
		return matrix.mul(this, B, C);
	}
	
	public Matrix<N> mul(Matrix<N> B){
		return matrix.mul(this, B);
	}
	
	public Matrix<N> mul(Number value){
		return matrix.mul(this, value);
	}
	
	public Matrix<N> mul(Number value, Matrix<N> C){
		return matrix.mul(this, value, C);
	}
	
	public Matrix<N> plusni(Matrix<N> B){
		return matrix.plusni(this, B);
	}

	public Matrix<N> plusn(Matrix<N> B){
		return matrix.plusn(this, B);
	}
	
	public Matrix<N> plusi(Matrix<N> B){
		return matrix.plusi(this, B);
	}
	
	public Matrix<N> plusi(Number value){
		return matrix.plusi(this, value);
	}
		
	public Matrix<N> plus(Matrix<N> B, Matrix<N> C){
		return matrix.plus(this, B, C);
	}
	
	public Matrix<N> plus(Matrix<N> B){
		return matrix.plus(this, B);
	}
	
	public Matrix<N> plus(Number value){
		return matrix.plus(this, value);
	}
	
	public Matrix<N> plus(Number value, Matrix<N> C){
		return matrix.plus(this, value, C);
	}		
	
	public Matrix<N> minusni(Matrix<N> B){
		return matrix.minusni(this, B);
	}
	
	public Matrix<N> minusn(Matrix<N> B){
		return matrix.minusn(this, B);
	}
	
	public Matrix<N> minusi(Matrix<N> B){
		return matrix.minusi(this, B);
	}
	
	public Matrix<N> minusi(Number value){
		return matrix.minusi(this, value);
	}
	
	public Matrix<N> minus(Matrix<N> B, Matrix<N> C){
		return matrix.minus(this, B, C);
	}
	
	public Matrix<N> minus(Matrix<N> B){
		return matrix.minus(this, B);
	}
	
	public Matrix<N> minus(Number value){
		return matrix.minus(this, value);
	}
	
	public Matrix<N> minus(Number value, Matrix<N> C){
		return matrix.minus(this, value, C);
	}
	
	public Matrix<N> rdivi(Number value){
		return matrix.rdivi(this, value);
	}
	
	public Matrix<N> rdiv(Number value){
		return matrix.rdiv(this, value);
	}
	
	public Matrix<N> rdiv(Number value, Matrix<N> C){
		return matrix.rdiv(this, value, C);
	}	
	
	public Matrix<N> divi(Number value){
		return matrix.divi(this, value);
	}
	
	public Matrix<N> divi(Matrix<N> B){
		return matrix.divi(this, B);
	}
	
	public Matrix<N> div(Matrix<N> B, Matrix<N> C){
		return matrix.div(this, B, C);
	}
	
	public Matrix<N> div(Matrix<N> B){
		return matrix.div(this, B);
	}
	
	public Matrix<N> div(Number value){
		return matrix.div(this, value);
	}
	
	public Matrix<N> div(Number value, Matrix<N> C){
		return matrix.div(this, value, C);
	}

	public Matrix<N> dot(Matrix<N> B){
		return matrix.dot(this, B);
	}
	
	public Matrix<N> dot(Matrix<N> B, Matrix<N> C){
		return matrix.dot(this, B, C);
	}
	
	public Matrix<N> dot(Number value, Matrix<N> B){
		return matrix.dot(this, value, B);
	}
	
	public Matrix<N> dot(Number value, Matrix<N> B, Matrix<N> C){
		return matrix.dot(this, value, B, C);
	}
	
	public Matrix<N> Tdot(Matrix<N> B){
		return matrix.Tdot(this, B);
	}
	
	public Matrix<N> Tdot(Matrix<N> B, Matrix<N> C){
		return matrix.Tdot(this, B, C);
	}
	
	public Matrix<N> Tdot(Number value, Matrix<N> B){
		return matrix.Tdot(this, value, B);
	}
	
	public Matrix<N> Tdot(Number value, Matrix<N> B, Matrix<N> C){
		return matrix.Tdot(this, value, B, C);
	}
	
	public Matrix<N> dotT(Matrix<N> B){
		return matrix.dotT(this, B);
	}
	
	public Matrix<N> dotT(Matrix<N> B, Matrix<N> C){
		return matrix.dotT(this, B, C);
	}
	
	public Matrix<N> dotT(Number value, Matrix<N> B){
		return matrix.dotT(this, value, B);
	}
	
	public Matrix<N> dotT(Number value, Matrix<N> B, Matrix<N> C){
		return matrix.dotT(this, value, B, C);
	}	
	
	public Matrix<N> TdotT(Matrix<N> B){
		return matrix.TdotT(this, B);
	}
	
	public Matrix<N> TdotT(Matrix<N> B, Matrix<N> C){
		return matrix.TdotT(this, B, C);
	}
	
	public Matrix<N> TdotT(Number value, Matrix<N> B){
		return matrix.TdotT(this, value, B);
	}
	
	public Matrix<N> TdotT(Number value, Matrix<N> B, Matrix<N> C){
		return matrix.TdotT(this, value, B, C);
	}
	
	public Matrix<N> negi(){
		return matrix.negi(this);
	}
	
	public Matrix<N> neg(){
		return matrix.neg(this);
	}
	
	public Matrix<N> neg(Matrix<N> C){
		return matrix.neg(this, C);
	}
	
	public Matrix<N> mean(){
		return matrix.mean(this);
	}
	
	public Matrix<N> mean(int axis, Matrix<N> C){
		return matrix.mean(this, axis, C);
	}
	
	public Matrix<N> mean(int axis){
		return matrix.mean(this, axis);
	}	
	
	public Matrix<N> meann(){
		return matrix.meann(this);
	}
	
	public Matrix<N> meann(int axis){
		return matrix.meann(this, axis);
	}
	
	public Matrix<N> pow2i(){
		return matrix.pow2i(this);
	}
	
	public Matrix<N> pow2(){
		return matrix.pow2(this);
	}
	
	public Matrix<N> pow2(Matrix<N> C) {
		return matrix.pow2(this, C);
	}
	
	public Matrix<N> powi(Number exponent){
		return matrix.powi(this, exponent);
	}
	
	public Matrix<N> pow(Number exponent){
		return matrix.pow(this, exponent);
	}
	
	public Matrix<N> pow(Number exponent, Matrix<N> C){
		return matrix.pow(this, exponent, C);
	}
	
	public Matrix<N> sqrti(){
		return matrix.sqrti(this);
	}
	
	public Matrix<N> sqrt(){
		return matrix.sqrt(this);
	}
	
	public Matrix<N> sqrt(Matrix<N> C){
		return matrix.sqrt(this, C);			
	}
	
	public Matrix<N> expi(){
		return matrix.expi(this);
	}
		
	public Matrix<N> exp(){
		return matrix.exp(this);
	}
	
	public Matrix<N> exp(Matrix<N> C){
		return matrix.exp(this, C);
	}
	
	public Matrix<N> tanhi(){
		return matrix.tanhi(this);
	}
	
	public Matrix<N> tanh(){
		return matrix.tanh(this);
	}
	
	public Matrix<N> tanh(Matrix<N> C){
		return matrix.tanh(this, C);
	}
	
	public Matrix<N> var(int axis, int d){
		return matrix.var(this, axis, d);
	}
	
	public Matrix<N> varn(int axis, int d){
		return matrix.varn(this, axis, d);
	}
	
	public Matrix<N> std(int axis, int d, Matrix<N> C){
		return matrix.std(this, axis, d, C);
	}
	
	public Matrix<N> std(int axis, int d){
		return matrix.std(this, axis, d);
	}
	
	public Matrix<N> stdn(int axis, int d){
		return matrix.stdn(this, axis, d);
	}
	
	public Matrix<N> max(){
		return matrix.max(this);
	}
	
	public Matrix<N> max(int axis){
		return matrix.max(this, axis);
	}
	
	public Matrix<N> max(Number value, Matrix<N> C){
		return matrix.max(this, value, C);		
	}
	
	public Matrix<N> maxi(Number value){
		return matrix.maxi(this, value);
	}	
	
	public Matrix<N> min(){
		return matrix.min(this);
	}
	
	public Matrix<N> min(int axis){
		return matrix.min(this, axis);
	}
	
	public Matrix<N> min(Number value, Matrix<N> C){
		return matrix.min(this, value, C);
	}
	
	public Matrix<N> argmax(int axis){
		return matrix.argmax(this, axis);
	}
	
	public Matrix<N> argmin(int axis){
		return matrix.argmin(this, axis);
	}
	
	public Matrix<N> sigmoid(Matrix<N> C){
		return matrix.sigmoid(this, C);
	}
	
	public Matrix<N> sigmoidi(){
		return matrix.sigmoidi(this);
	}
	
	// Statics
	
	public static Matrix<double[]> rand(int numRows, int numColumns){
		Matrix<double[]> C = new Matrix<>(double[].class, numRows, numColumns);		
		for (int row = 0; row < numRows; row++) 
		   for (int col = 0; col < numColumns; col++)
		    C.getData()[row + col * numRows] = Random.instance.nextDouble();		
		return C;		
	}
	
	public static Matrix<double[]> randn(int numRows, int numColumns){
		Matrix<double[]> C = new Matrix<>(double[].class, numRows, numColumns);		
		for (int row = 0; row < numRows; row++) 
		   for (int col = 0; col < numColumns; col++)
		    C.getData()[row + col * numRows] = Random.instance.nextGaussian();		
		return C;		
	}
	
	public static Matrix<double[]> ones(int numRows, int numColumns) {	
		Matrix<double[]> C = new Matrix<>(double[].class, numRows, numColumns);		
		C.one();
		return C;		
	}
	
	public static Matrix<double[]> zeros(int numRows, int numColumns) {	
		Matrix<double[]> C = new Matrix<>(double[].class, numRows, numColumns);		
		return C;		
	}
	
	public static void main(String args[]) {		
		
		double featuresM[][] = {{5.1, 3.5, 1.4, 0.2, 5.1, 3.5, 1.4, 0.2},
								{5.9, 3. , 5.1, 1.8, 5.9, 3. , 5.1, 1.8},
								{5.5, 2.4, 3.8, 1.1, 5.5, 2.4, 3.8, 1.1},
								{6.3, 3.3, 6. , 2.5, 6.3, 3.3, 6. , 2.5}};
		
		Matrix<double[]> m = new Matrix<>(featuresM);
		m.varn(1, 1).print();
	}	
	
}
