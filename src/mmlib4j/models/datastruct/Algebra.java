package mmlib4j.models.datastruct;

import com.github.fommil.netlib.BLAS;

public class Algebra {
	
    public static Matrix<float[]> multAdd(float alpha, Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {       

        float[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().sgemm("N","N", C.numRows(), C.numColumns(),
                A.numColumns(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }
    
    public static Matrix<double[]> multAdd(double alpha, Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {       

        double[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().dgemm("N","N", C.numRows(), C.numColumns(),
                A.numColumns(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }

    public static Matrix<float[]> transAmultAdd(float alpha, Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {

        float[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().sgemm("T", "N", C.numRows(), C.numColumns(),
                A.numRows(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }
    
    public static Matrix<double[]> transAmultAdd(double alpha, Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {

        double[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().dgemm("T", "N", C.numRows(), C.numColumns(),
                A.numRows(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }
    
    
    public static Matrix<float[]> transBmultAdd(float alpha, Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {

        float[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().sgemm("N", "T", C.numRows(), C.numColumns(),
                A.numColumns(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }
    
    public static Matrix<double[]> transBmultAdd(double alpha, Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {

        double[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().dgemm("N", "T", C.numRows(), C.numColumns(),
                A.numColumns(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }  

    public static Matrix<float[]> transABmultAdd(float alpha, Matrix<float[]> A, Matrix<float[]> B, Matrix<float[]> C) {

        float[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().sgemm("T", "T", C.numRows(), C.numColumns(),
                A.numRows(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }
    
    public static Matrix<double[]> transABmultAdd(double alpha, Matrix<double[]> A, Matrix<double[]> B, Matrix<double[]> C) {

        double[] Ad = A.getData(), Bd = B.getData(), Cd = C.getData();

        BLAS.getInstance().dgemm("T", "T", C.numRows(), C.numColumns(),
                A.numRows(), alpha, Ad, Math.max(1, A.numRows()), Bd,
                Math.max(1, B.numRows()), 1, Cd, Math.max(1, C.numRows()));

        return C;
    }
    
    public static Matrix<double[]> dplus(int size, Matrix<double[]> A, Matrix<double[]> B) {    	
    	BLAS.getInstance().daxpy(size, 1, B.getData(), 0, 1, A.getData(), 0, 1);
    	return A;    	
    }    
    
    public static Matrix<float[]> fplus(int size, Matrix<float[]> A, Matrix<float[]> B) {    	
    	BLAS.getInstance().saxpy(size, 1, B.getData(), 0, 1, A.getData(), 0, 1);
    	return A;    	
    } 
    
    public static Matrix<double[]> dminus(int size, Matrix<double[]> A, Matrix<double[]> B) {       	
    	BLAS.getInstance().daxpy(size, -1, B.getData(), 0, 1, A.getData(), 0, 1);
    	return A;    	
    }
	
    public static Matrix<float[]> fminus(int size, Matrix<float[]> A, Matrix<float[]> B) {       	
    	BLAS.getInstance().saxpy(size, -1, B.getData(), 0, 1, A.getData(), 0, 1);
    	return A;    	
    }
    
}
