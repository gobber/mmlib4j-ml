package mmlib4j.models.preprocessing;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;

/**
 * 
 * <p> This class implements the min max normalization for the {@link MatrixOld} datastruct. </p>
 * 
 * @author Charles Ferreira Gobber
 * @version  0.0.1
 * @since  0.0.1
 * @see Scaler 
 * @see Zscore
 * @see MatrixOld  
 * 
 */

public class MinMax<N> extends Scaler<N>{

	/**
	 * 
	 * 	<p>This method normalizes a given Matrix :math:`A` by min max scaler. More formally, the 
	 * 	min max normalization can be defined as follows: </p> 
	 * 
	 * 	.. math::
	 * 		
	 * 		\begin{aligned}
	 * 		C_{1c} = \frac{A_{1c} - \min\limits_{r=1, \ldots, m}\{A_{rc}\}}{ \max\limits_{r=1, \ldots, m}\{A_{rc}\} - \min\limits_{r=1, \ldots, m}\{A_{rc}\}}, c=1,\ldots,n,
	 * 		\end{aligned} 		
	 *
	 * 	<p> where :math:`m` is the number of rows and :math:`n` is the number of columns of :math:`A`. </p>	 
	 * 
	 *	@param matrix A Matrix
	 *	@return A matrix :math:`C` normalized by min max.
	 * 
	 */
	
	private interface Statistics<N>{
		N max();
		N min();
		int minLenght();
		int maxLenght();
	}
	
	private class StatisticsFloat64 implements Statistics<double[]>{		
		double max[];
		double min[];
		@Override
		public double[] max() {
			return max;
		}
		@Override
		public double[] min() {
			return min;
		}
		@Override
		public int minLenght() {
			return min.length;
		}
		@Override
		public int maxLenght() {
			return max.length;
		}		
	}
	
	private class StatisticsFloat32 implements Statistics<float[]>{
		float max[];
		float min[];
		@Override
		public float[] max() {
			return max;
		}
		@Override
		public float[] min() {
			return min;
		}
		@Override
		public int minLenght() {
			return min.length;
		}
		@Override
		public int maxLenght() {
			return max.length;
		}		
	}
	
	private Matrix<N> min;
	private Matrix<N> max;
	
	private Statistics<N> statistics;
	private Class<N> scalerType;
	
	public MinMax(Matrix<N> min, Matrix<N> max) {
		this.min = min;
		this.max = max;
	}
	
	public MinMax() {}
	
	public MinMax(Class<N> scalerType, String modelData) {	
		this.scalerType = scalerType;
		load(modelData);
	}	
	
	public MinMax(Class<N> scalerType, JsonObject jsonObject) {
		this.scalerType = scalerType;
		load(jsonObject);
	}
	
	public MinMax<N> load(String modelData) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();
        load(jsonObject);
        return this;
	}
	
	@SuppressWarnings("unchecked")
	public MinMax<N> load(JsonObject jsonObject) {	   			
		switch (scalerType.getCanonicalName()) {
		case "double[]":				
			this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat64.class);
			break;
		case "float[]":
			this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat32.class);
			break;				
		}				
		this.min = new Matrix<>(scalerType, 1, statistics.minLenght(), statistics.min(), false);		
        this.max = new Matrix<>(scalerType, 1, statistics.maxLenght(), statistics.max(), false);        
        return this;        
	}	
		
	public Matrix<N> normalize(Matrix<N> x) {				
		this.min = this.min == null ? x.min(0) : this.min;
		this.max = this.max == null ? x.max(0) : this.max;	
		//Matrix z = x.minus(min).divi(max.minus(min));		
		/*if(statistics.upper != 1 && statistics.lower != 0) {
			z.muli(statistics.upper-statistics.lower).plusi(statistics.lower);
		}*/		
		return x.minus(min).divi(max.minus(min));
	}

}
