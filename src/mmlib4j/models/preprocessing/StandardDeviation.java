package mmlib4j.models.preprocessing;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;

public final class StandardDeviation<N> extends Scaler<N>{
	
	private interface Statistics<N>{
		N std();
		int length();
	}
	
	private class StatisticsFloat64 implements Statistics<double[]>{
		double std[];
		@Override
		public double[] std() {
			return std;
		}
		@Override
		public int length() {
			return std.length;
		}		
		
	}
	
	private class StatisticsFloat32 implements Statistics<float[]>{
		float std[];
		@Override
		public float[] std() {
			return std;
		}
		@Override
		public int length() {
			return std.length;
		}		
	}
	
	private Statistics<N> statistics;
	private Class<N> scalerType;
	private Matrix<N> std;
	
	public StandardDeviation(){}
	
	public StandardDeviation(Class<N> scalerType, String modelData) {
		this.scalerType = scalerType;
		load(modelData);
	}	
	
	public StandardDeviation(Class<N> scalerType, JsonObject jsonObject) {
		this.scalerType = scalerType;
		load(jsonObject);
	}
	
	public StandardDeviation(Matrix<N> std){
		this.std = std;
	}

	public StandardDeviation<N> load(String modelData) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();
        load(jsonObject);
        return this;
	}
	
	@SuppressWarnings("unchecked")
	public StandardDeviation<N> load(JsonObject jsonObject) {			
		switch (scalerType.getCanonicalName()) {
			case "double[]":				
				this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat64.class);
				break;
			case "float[]":
				this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat32.class);
				break;				
		}		
        this.std = new Matrix<>(scalerType, 1, statistics.length(), statistics.std(), false);        
        return this;
	}
	
	public Matrix<N> normalize(Matrix<N> x) {
		this.std = this.std  == null ? x.stdn(0, 1) : this.std;
		return x.div(std);
	}
	
}
