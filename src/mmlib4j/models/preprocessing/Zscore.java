package mmlib4j.models.preprocessing;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;


/**
 * 
 * <p> This class implements the zscore normalization for the {@link MatrixOld} datastruct. </p>
 * 
 * @author Charles Ferreira Gobber
 * @version  0.0.1
 * @since  0.0.1
 * @see Scaler 
 * @see Zscore
 * @see MatrixOld  
 * 
 */

public class Zscore<N> extends Scaler<N> {

	/**
	 * 
	 * 	<p>This method normalizes a given Matrix :math:`A` by zscore scaler. More formally, the 
	 * 	zscore normalization can be defined as follows: </p> 
	 * 
	 * 	.. math::
	 * 		
	 * 		\begin{aligned}
	 * 		C_{1c} = \frac{A_{1c} - \mu_{1c} }{ \sqrt{\frac{1}{m-1}\sum\limits_{r=1}^m (A_{rc} - \mu_{1c})^2 } }, c=1,\ldots,n,
	 * 		\end{aligned}	
	 *
	 * 	<p> where :math:`m` is the number of rows, :math:`n` is the number of columns of :math:`A` and :math:`\mu_{r1}` is the column mean of :math:`A`. </p>	 
	 * 
	 *	@param matrix A Matrix
	 *	@return A matrix :math:`C` normalized by zscore.
	 * 
	 */
	
	private interface Statistics<N>{
		N mean();
		N std();
		int meanLenght();
		int stdLenght();
	}
	
	private class StatisticsFloat64 implements Statistics<double[]>{		
		double mean[];
		double std[];		
		@Override
		public double[] mean() {
			return mean;
		}
		@Override
		public double[] std() {
			return std;
		}
		@Override
		public int meanLenght() {
			return mean.length;
		}
		@Override
		public int stdLenght() {
			return std.length;
		}		
	}
	
	private class StatisticsFloat32 implements Statistics<float[]>{		
		float mean[];
		float std[];		
		@Override
		public float[] mean() {
			return mean;
		}
		@Override
		public float[] std() {
			return std;
		}
		@Override
		public int meanLenght() {
			return mean.length;
		}
		@Override
		public int stdLenght() {
			return std.length;
		}		
	}
	
	private Statistics<N> statistics;
	private Class<N> scalerType;
	private Matrix<N> mean;
	private Matrix<N> std;
	
	public Zscore(){}
	
	public Zscore(Class<N> scalerType, String modelData) {
		this.scalerType = scalerType;
		load(modelData);
	}	
	
	public Zscore(Class<N> scalerType, JsonObject jsonObject) {
		this.scalerType = scalerType;
		load(jsonObject);
	}		

	public Zscore(Matrix<N> mean, Matrix<N> std){
		this.mean = mean;
		this.std  = std;	
	}
	
	public Zscore<N> load(String modelData) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();
        load(jsonObject);
        return this;
	}
	
	@SuppressWarnings("unchecked")
	public Zscore<N> load(JsonObject jsonObject) {	    
		switch (scalerType.getCanonicalName()) {
		case "double[]":				
			this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat64.class);
			break;
		case "float[]":
			this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat32.class);
			break;				
		}				
        this.mean = new Matrix<>(scalerType, 1, statistics.meanLenght(), statistics.mean(), false);
        this.std = new Matrix<>(scalerType, 1, statistics.stdLenght(), statistics.std(), false);        
        return this;
	}
	
	public Matrix<N> normalize(Matrix<N> x) {		
		this.mean = this.mean == null ? x.meann(0)   : this.mean;
		this.std  = this.std  == null ? x.stdn(0, 1) : this.std;						
		return x.minus(mean).divi(std);				
	}
	
}
