package mmlib4j.models.preprocessing;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;

public class Mean<N> extends Scaler<N>{
	
	private interface Statistics<N>{
		N mean();
		int meanLenght();
	}
	
	private class StatisticsFloat64 implements Statistics<double[]>{
		double mean[];
		@Override
		public double[] mean() {
			return mean;
		}
		@Override
		public int meanLenght() {
			return mean.length;
		}				
	}
	
	private class StatisticsFloat32 implements Statistics<float[]>{
		float mean[];
		@Override
		public float[] mean() {
			return mean;
		}
		@Override
		public int meanLenght() {
			return mean.length;
		}		
	}
	
	private Statistics<N> statistics;
	private Class<N> scalerType;
	private Matrix<N> mean;
	
	public Mean() {}
	
	public Mean(Class<N> scalerType, String modelData) {
		this.scalerType = scalerType;
		load(modelData);		
	}	
	
	public Mean(Class<N> scalerType, JsonObject jsonObject) {
		this.scalerType = scalerType;
		load(jsonObject);
	}
	
	public Mean(Matrix<N> mean){
		this.mean = mean;
	}
	
	public Mean<N> load(String modelData) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
		String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();
        load(jsonObject);        
        return this;
	}
	
	@SuppressWarnings("unchecked")
	public Mean<N> load(JsonObject jsonObject) {		
		switch (scalerType.getCanonicalName()) {
		case "double[]":				
			this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat64.class);
			break;
		case "float[]":
			this.statistics = new Gson().fromJson(jsonObject, StatisticsFloat32.class);
			break;				
		}		
        this.mean = new Matrix<>(scalerType, 1, statistics.meanLenght(), statistics.mean(), false);        
        return this;
	}
	
	@Override
	public Matrix<N> normalize(Matrix<N> x) {
		this.mean = this.mean == null ? x.meann(0) : this.mean;
		return x.minus(mean);
	}

}
