package mmlib4j.models.preprocessing;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mmlib4j.models.datastruct.Matrix;


/**
 * 
 * <p> This interface is a model to normalization classes for the {@link MatrixOld} datastruct. </p>
 * 
 * @author Charles Ferreira Gobber
 * @version  0.0.1
 * @since  0.0.1
 * @see MinMax
 * @see Zscore 
 * @see MatrixOld  
 * 
 */

public class Scaler<N> {
	
	/**
	 * 	
	 *	@param matrix A Matrix
	 *	@return A matrix :math:`C` normalized. 
	 * 
	 */
	
	private Scaler<N> scaler;
	private Class<N> scalerType;
	
	Scaler(){}
	
	public Scaler(Class<N> scalerType, String modelData) {
		this.scalerType = scalerType;
		load(modelData);
	}
	
	public Scaler(Class<N> scalerType, JsonObject jsonObject){
		this.scalerType = scalerType;
		load(jsonObject);
	}
	
	public Scaler<N> load(String modelData) {		
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(modelData));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	    
        String jsonStr = scanner.useDelimiter("\\Z").next(); 
        scanner.close();
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(jsonStr).getAsJsonObject();        
        return load(jsonObject); 		
	}
	
	public Scaler<N> load(JsonObject jsonObject) {
		String type = jsonObject.get("type").getAsString();
		if(type.contains("ZscoreScaler")) {  
        	scaler = new Zscore<>(scalerType, jsonObject);
        } else if(type.contains("StandardDeviationScaler")){
        	scaler = new StandardDeviation<>(scalerType, jsonObject);        	
        } else if(type.contains("MeanScaler")) {
        	scaler = new Mean<>(scalerType, jsonObject);
        }else if(type.contains("MinMaxScaler")){
        	scaler = new MinMax<>(scalerType, jsonObject);
        } else {
        	scaler = new IdentityScaler<>();
        } 	
		return this; 		
	}
	
	public Matrix<N> normalize(Matrix<N> A) {
		return scaler.normalize(A);
	}
	
}
