package mmlib4j.models.preprocessing;
import mmlib4j.models.datastruct.Matrix;

public class IdentityScaler<N> extends Scaler<N>{	
	
	@Override
	public Matrix<N> normalize(Matrix<N> A) {
		return A;
	}

}
