package mmlib4j.files;

import java.io.File;

public interface DataGroup {
	
	public boolean isgroup(File file);

}
