package mmlib4j.files;

import java.io.File;
import java.util.Arrays;

public class GroupByNumber implements DataGroup {

	private String lastFileId = "";		
	
	@Override
	public boolean isgroup(File file) {
		
		String removeNumbers = file.getName().replaceAll("[^0-9]+", ";"); 	
		String fileid = String.join("", Arrays.asList(removeNumbers.trim().split(";")));																	
		
		if(fileid.equals(lastFileId)) {									
			return true;			
		}
		
		lastFileId = fileid;		
		return false;
		
	}

	
}
