package mmlib4j.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class DataLoader {
	
	private List<DataFile> listOfFiles;

	private String filesets[];
	
	private String inputPath = "";
	
	private String outputPath = "";
	
	private String extensions[] = {".gif", ".png", ".jpg", ".csv", ".txt"};
	
	private DataGroup dataGroup = new GroupByNumber();
	
	public DataLoader(String filesets[]) {
				
		this.filesets  = filesets;
		listOfFiles    = new ArrayList<>();
		
	}
	
	public DataLoader(String inputPath, String filesets[]) {
		
		this.inputPath  = inputPath;
		this.filesets   = filesets;		
		listOfFiles     = new ArrayList<>();
		
	}
	
	public DataLoader(String inputPath, String outputPath, String filesets[]) {
		
		this.inputPath  = inputPath;
		this.outputPath = outputPath;
		this.filesets   = filesets;		
		listOfFiles     = new ArrayList<>();
		
	}
	
	public boolean isValidExtension(String fileName) {
		
		for(String fileType : extensions) {			
			
			if(fileName.endsWith(fileType)) {
				
				return true;
				
			}
			
		}
		
		return false;		
		
	}	
	
	public static ArrayList<String> load(File file) {
		
		ArrayList<String> lines = new ArrayList<>();
		Scanner readfile = null;
		
		try {
			readfile = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		
		while (readfile.hasNextLine()) {			
			lines.add(readfile.nextLine());			
		}
		
		return lines;
		
	}
	
	public DataLoader dir() {
		
		for(String dataset : filesets) {
		
			File dir = new File( inputPath + dataset );
			
			File files[] = dir.listFiles();	
			
			Arrays.sort(files);
			
			List<File> filesToLoad = null;			
			
			for( File file : files ) {				
				
				if(isValidExtension(file.getName())) {					
					
					if(!dataGroup.isgroup(file)) {		
						filesToLoad = new ArrayList<>();					
						listOfFiles.add(new DataFile(dataset, outputPath, filesToLoad));						
					}
					
					filesToLoad.add(file);
									
				}
				
			}
			
		}
		
		return this;
		
	}
	
	public List<DataFile> getListOfFiles() {
		return listOfFiles;
	}

	public void setListOfFiles(List<DataFile> datasetsFiles) {
		this.listOfFiles = datasetsFiles;
	}	

}
