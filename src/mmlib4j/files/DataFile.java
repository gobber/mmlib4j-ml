package mmlib4j.files;

import java.io.File;
import java.util.List;

public class DataFile {
	
	private String dataName;
	private String outputPath;
	private List<File> files;
	
	public DataFile(String dataName, String outputPath, List<File> files) {			
		this.dataName   = dataName;
		this.files      = files;		
		this.outputPath = outputPath;
	}
	
	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}			
	
	public File filter(String pattern) {
		
		for(File f : files) {			
			if(f.getName().contains(pattern)) {				
				return f.getAbsoluteFile();				
			}			
		}
		
		return null;
		
	}
	
	public String prefix(String pattern) {
		
		File f = filter(pattern);		
		return f.getName().substring(0, f.getName().lastIndexOf("."));
		
	}
	
	public String output() {		
		return outputPath + dataName + "/";		
	}
	
}