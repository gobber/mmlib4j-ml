package mmlib4j.features;

import java.util.HashSet;

import mmlib4j.representation.tree.NodeLevelSets;
import mmlib4j.representation.tree.attribute.Attribute;
import mmlib4j.representation.tree.attribute.AttributeComputedIncrementally;
import mmlib4j.representation.tree.componentTree.ConnectedFilteringByComponentTree;
import mmlib4j.representation.tree.componentTree.NodeCT;

public class MeanVolumeFeatureR extends FeatureBody implements Feature {		
	
	private double somaR[];
	
	public MeanVolumeFeatureR(String featureName, int featureId){
		
		this.featureName = featureName;
		this.featureId   = featureId;
	}
	
	@Override
	public void loadFeature(ConnectedFilteringByComponentTree tree) {
		
		somaR = new double[tree.getNumNode()];			
		
		new AttributeComputedIncrementally() {									
			
			@Override
			public void preProcessing(NodeLevelSets v) {
				
				for(int p: v.getCanonicalPixels()){
					
					somaR[v.getId()] += imgRGB.getRed(p);
	
					
				}
				
			}
			@Override
			public void mergeChildren(NodeLevelSets parent, NodeLevelSets son) {
				
				somaR[parent.getId()] += somaR[son.getId()];		
				
			}

			@Override
			public void posProcessing(NodeLevelSets parent) { 
				
				somaR[parent.getId()] /= parent.getArea();							
				
			}
			
		}.computerAttribute( tree.getRoot() );		
	
		addAttributeInNodesCT( tree.getListNodes() );
		
	}
	
	public void addAttributeInNodesCT( HashSet<NodeCT> list ) {
		
		for( NodeCT node: list ) {
			
			addAttributeInNodes( node );
			
		}
		
	} 
	
	public void addAttributeInNodes( NodeLevelSets node ) {
		
		node.addAttribute( featureId,  new Attribute(featureId, somaR[ node.getId() ]));
		
	}	

}
