package mmlib4j.features;

import mmlib4j.representation.tree.attribute.Attribute;
import mmlib4j.representation.tree.attribute.ComputerMserComponentTree;
import mmlib4j.representation.tree.componentTree.ConnectedFilteringByComponentTree;
import mmlib4j.representation.tree.componentTree.NodeCT;

public class MserFeature extends FeatureBody implements Feature{	
	
	private int delta = 10;
	
	private double maxVariation = Double.MAX_VALUE;

	private int minArea = 0;
	
	private int maxArea = Integer.MAX_VALUE;
	
	private boolean estimateDelta = false;
	
	private int attribute = Attribute.AREA;	
	
	public MserFeature(int delta, String featureName, int featureId) {
		
		this.delta       = delta;
		this.featureName = featureName;
		this.featureId   = featureId;
		
		
	}
	
	public MserFeature(int delta, 
					   double maxVariation,						
					   int minArea,						
					   int maxArea,						
					   boolean estimateDelta,						
					   int attribute,
					   String featureName,
					   int featureId) {
		
		this.delta         = delta;
		this.maxVariation  = maxVariation;
		this.minArea       = minArea;		
		this.maxArea       = maxArea;		
		this.estimateDelta = estimateDelta;		
		this.attribute     = attribute;
		this.featureName   = featureName;
		this.featureId     = featureId;
		
	}

	@Override
	public void loadFeature(ConnectedFilteringByComponentTree tree) {
		
		ComputerMserComponentTree mser = new ComputerMserComponentTree(tree);
		mser.setMaxArea(maxArea);
		mser.setMinArea(minArea);
		mser.setEstimateDelta(estimateDelta);
		mser.setMaxVariation(maxVariation);
		mser.setAttribute(attribute);
		
		boolean nodesByMser[] = mser.getMappingNodesByMSER(delta);
		
		for(NodeCT node: tree.getListNodes()) {
			
			double v = nodesByMser[node.getId()] ? 1 : 0;
			node.addAttribute(featureId, new Attribute(featureId, v));
			
		}
		
	}		
	
}
