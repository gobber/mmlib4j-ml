package mmlib4j.features;

import mmlib4j.representation.tree.componentTree.ConnectedFilteringByComponentTree;

/**
 * 
 * 	Load Features (Attributes in tree) that are not parameterized 
 * 	(all that are included in loadAttribute of ConnectedFilteringByComponentTree)
 * 	
 * */

public class NormalFeature extends FeatureBody implements Feature {		
	
	public NormalFeature(String featureName, int featureId) {
		
		this.featureName = featureName;		
		this.featureId   = featureId;
		
	}

	public void loadFeature(ConnectedFilteringByComponentTree tree) {
		
		tree.loadAttribute(featureId);
		
	}

}
