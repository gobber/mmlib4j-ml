package mmlib4j.features;

import mmlib4j.images.ColorImage;
import mmlib4j.representation.tree.componentTree.ConnectedFilteringByComponentTree;

public interface Feature {
	
	public String getFeatureName();
	
	public int getFeatureId();
	
	public ColorImage getImgRGB();
	
	public void setImgRGB(ColorImage imgRGB);
	
	void loadFeature(ConnectedFilteringByComponentTree tree);
	
}
