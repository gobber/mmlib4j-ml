package mmlib4j.features;

import java.io.File;
import java.util.ArrayList;

import mmlib4j.debug.Debug;
import mmlib4j.files.DataWriter;
import mmlib4j.images.ColorImage;
import mmlib4j.models.datastruct.Matrix;
import mmlib4j.representation.tree.attribute.Attribute;
import mmlib4j.representation.tree.componentTree.ConnectedFilteringByComponentTree;
import mmlib4j.representation.tree.componentTree.NodeCT;
import mmlib4j.targets.Target;

public class ExtractFeatures {
		
	private ArrayList<NodeCT>[] ascendantsPaths;	
	private ArrayList<NodeCT>[] descendantsPaths;	
	private double[] sumAttributeOfDescPath;	
	private int delta; 	
	private int cont;	
	private boolean maxNodesNr[];	
	private int accumulatedAttributeInPath = Attribute.AREA;
	private ConnectedFilteringByComponentTree tree;
	private Feature features[];		
	private Target target;	
	private ColorImage imgRGB;	
	private String ascPrefix   = "Asc_";
	private String descPrefix  = "Desc_";	
	private String nodePrefix  = "Node_";	
	private String outputName  = "Matching";	
	private DataWriter dataWriter = new DataWriter();	
	private int numValidNodes;	
	private int numberOfFeatures;
	
	public ExtractFeatures(ConnectedFilteringByComponentTree tree, 
						   ColorImage imgRGB,
						   boolean maxNodesNr[],
						   int numValidNodes,
						   Feature features[], 
						   Target target) {		

		this.tree          = tree;		
		this.maxNodesNr    = maxNodesNr;
		this.numValidNodes = numValidNodes;
		this.features      = features;				
		this.imgRGB        = imgRGB;
		this.target 	   = target;

	}
	
	public ExtractFeatures(ConnectedFilteringByComponentTree tree, 
						   ColorImage imgRGB,
			   			   boolean maxNodesNr[],
			   			   int numValidNodes,
			   			   Feature features[]) {		
		
		this.tree          = tree;		
		this.maxNodesNr	   = maxNodesNr;
		this.numValidNodes = numValidNodes;
		this.features      = features;				
		this.imgRGB        = imgRGB;
		
	}

	
	public ExtractFeatures(ConnectedFilteringByComponentTree tree, 
			   			   boolean maxNodesNr[],
			   			   int numValidNodes,
			   			   Feature features[]) {
		

		this.tree          = tree;			
		this.maxNodesNr    = maxNodesNr;
		this.numValidNodes = numValidNodes;
		this.features      = features;			

	}
	
	public ExtractFeatures load() {
		
		Debug.getInstance().start();
		Debug.getInstance().show("Loading features...\n");
		
		for(Feature feature : features) {						
			feature.setImgRGB(imgRGB);
			feature.loadFeature(tree);			
		}
		
		Debug.getInstance().show("Loaded features");
		Debug.getInstance().end();
				
		return this;
		
	}
	
	public ExtractFeatures find(int delta) {
		
		this.delta            = delta;
		this.numberOfFeatures = ((2*delta)+1)*features.length;
		
		Debug.getInstance().start();
		Debug.getInstance().show("Loading paths...\n");
		
		ascendantsPaths        = new ArrayList[tree.getNumNode()];
		descendantsPaths       = new ArrayList[tree.getNumNode()];
		sumAttributeOfDescPath = new double[tree.getNumNode()];
		
		for(NodeCT node: tree.getListNodes()){									
			ArrayList<NodeCT> ascendantPath = getNodesAscendantsPath(node, delta);
			ascendantsPaths[node.getId()] = ascendantPath;
			maxAttributeDescendantsPath(ascendantPath.get(ascendantPath.size()-1), node);			
		}
					
		sumAttributeOfDescPath = null;
		
		Debug.getInstance().show("Loaded paths");
		Debug.getInstance().end();
		
		return this;
		
	}	
	
	public String header(String separator) {
		
		String header = "";
		String cell   = "";		
		int dir = -1;
		
		for(int i = delta ; i <= delta ; i += dir) {			
			if(dir == 1) {				
				cell = ascPrefix  + i + "_";				
			} else {				
				cell = descPrefix + i + "_";				
			}			
			if(i == 0) {
				dir  = 1;
				cell = nodePrefix;
			}			
			for(Feature feature : features) {				
				header += cell + feature.getFeatureName() + separator;				
			}			
		}				
		
		header += outputName;		
		return header;
		
	}
	
	private void extract(NodeCT node, Matrix<double[]> matrix, int row) {
		
		cont = 0;		
		featureExtractionOfAscendant(node, matrix, row);		
		featureExtractionOfDescendant(node, matrix, row);				
		
	}
	
	public Matrix<double[]> x (NodeCT node) {		
				
		Matrix<double[]> x = new Matrix<double[]>(double[].class, 1, numberOfFeatures);		
		extract(node, x, 0);		
		return x;
		
	}
	
	public Matrix<double[]> y (NodeCT node) {
		
		Matrix<double[]> y = new Matrix<double[]>(double[].class, 1, 1);		
		y.getData()[0] = target.match(node);		
		return y;
		
	}	
	
	public Matrix<double[]> Y() {
		
		Matrix<double[]> Y = new Matrix<double[]>(double[].class, numValidNodes, 1);
		int row = 0;
		
		Debug.getInstance().start();
		Debug.getInstance().show("Extracting set Y...\n");
		
		for(NodeCT node : tree.getListNodes()) {			
			if(maxNodesNr[node.getId()]) {
				Y.getData()[row] = target.match(node);						
				row++;
			}		
		}
		
		Debug.getInstance().show("Extracted set Y");
		Debug.getInstance().end();
		
		return Y;
		
	}
	
	public Matrix<double[]> X() {
		
		Matrix<double[]> X = new Matrix<double[]>(double[].class, numValidNodes, numberOfFeatures);
		int row = 0;
		
		Debug.getInstance().start();
		Debug.getInstance().show("Extracting set X...\n");
		
		for(NodeCT node : tree.getListNodes()) {			
			if(maxNodesNr[node.getId()]) {							
				extract(node, X, row);						
				row++;
			}		
		}
		
		Debug.getInstance().show("Extracted set X");
		Debug.getInstance().end();
		
		return X;
		
	}
	
	public Matrix<double[]> samples () {
			
		Matrix<double[]> samples = new Matrix<double[]>(double[].class, numValidNodes, numberOfFeatures+1);
		int row = 0;
		
		Debug.getInstance().start();
		Debug.getInstance().show("Extracting features...\n");
		
		for(NodeCT node : tree.getListNodes()) {			
			if(maxNodesNr[node.getId()]) {							
				extract(node, samples, row);					
				samples.getData()[row + (samples.numColumns()-1) * samples.numRows()] = target.match(node);				
				//samples.set(row, samples.numColumns()-1, target.match(node));				
				row++;
			}		
		}
		
		Debug.getInstance().show("Extracted features");
		Debug.getInstance().end();
		
		return samples;
		
	}
	
	public Matrix<double[]> sample (NodeCT node) {	
		
		Matrix<double[]> sample = new Matrix<double[]>(double[].class, 1, numberOfFeatures+1);		
		extract(node, sample, 0);	
		sample.getData()[sample.numColumns()-1] = target.match(node);				
		//sample.set(0, sample.numColumns()-1, target.match(node));
		
		return sample;
		
	}
	
	public void save(String outputDir, String fileName) {
		
		Debug.getInstance().start();
		Debug.getInstance().show("Extracting features...\n");
		
		File outdir = new File(outputDir);		
		
		if(!outdir.exists()) {
			outdir.mkdirs();
		}
			
		dataWriter.create(outputDir + fileName);						
		dataWriter.write(header(dataWriter.getSeparator()));
		
		for(NodeCT node : tree.getListNodes()) {			
			if(maxNodesNr[node.getId()]) {								
				dataWriter.write(sample(node));				
			}			
		}
		
		Debug.getInstance().show("Saved features into " + fileName + dataWriter.getExtension());
		Debug.getInstance().end();
		
		dataWriter.close();		
		
	}
	
	public ArrayList<NodeCT> getNodesAscendantsPath(NodeCT node, int h){
		
		NodeCT n = node;
		
		ArrayList<NodeCT> ascendantPath = new ArrayList<NodeCT>();		
		ascendantPath.add(node);		
		sumAttributeOfDescPath[node.getId()] += node.getAttributeValue(accumulatedAttributeInPath);
		
		for(int i=0; i <= h; i++){
			if(node.isMaxtree()){
				if(node.getLevel() >= n.getLevel() + h) {
					return ascendantPath;
				}
			}else{
				if(node.getLevel() <= n.getLevel() - h) {				
					return ascendantPath;
				}
			}
			if(n.getParent() != null) {
				n = n.getParent();
				ascendantPath.add(n);
				sumAttributeOfDescPath[node.getId()] += n.getAttributeValue(accumulatedAttributeInPath);
			}
			else {				
				return ascendantPath;
			}
		}
		return ascendantPath;
	}
	 
	public void maxAttributeDescendantsPath(NodeCT nodeAsc, NodeCT nodeDes){
		
		if(descendantsPaths[nodeAsc.getId()] == null) {
			descendantsPaths[nodeAsc.getId()]  = ascendantsPaths[nodeDes.getId()];
			sumAttributeOfDescPath[nodeAsc.getId()] = sumAttributeOfDescPath[nodeDes.getId()]; 
		}
			
		if(sumAttributeOfDescPath[nodeAsc.getId()] < sumAttributeOfDescPath[nodeDes.getId()]) {
			descendantsPaths[nodeAsc.getId()] = ascendantsPaths[nodeDes.getId()];
			sumAttributeOfDescPath[nodeAsc.getId()] = sumAttributeOfDescPath[nodeDes.getId()];
		}
			
	}
	
	public void featureExtractionOfAscendant(NodeCT node, Matrix<double[]>  matrix, int row) {				
		
		for(NodeCT n : ascendantsPaths[node.getId()]) {
			for( Feature feature : features ) {				
				matrix.getData()[row + cont * matrix.numRows()] = n.getAttributeValue(feature.getFeatureId());				
				//matrix.set(row, cont, n.getAttributeValue(feature.getFeatureId()));
				cont++;
			}
		}
		
		// put difference nodes repeating last node
		for(int i = 0 ; i <= delta-ascendantsPaths[node.getId()].size() ; i++ ) {
			for( Feature feature : features ) {		
				matrix.getData()[row + cont * matrix.numRows()] = ascendantsPaths[node.getId()].get(ascendantsPaths[node.getId()].size()-1).getAttributeValue(feature.getFeatureId());				
				//matrix.set(row, cont, ascendantsPaths[node.getId()].get(ascendantsPaths[node.getId()].size()-1).getAttributeValue(feature.getFeatureId()));
				cont++;
			}
		}		
	}
	
	public void  featureExtractionOfDescendant(NodeCT node, Matrix<double[]>  matrix, int row) {		
		// there is no descendant
		if(descendantsPaths[node.getId()] == null) {			
			for(int i = 0 ; i < delta ; i++ ) {
				for( Feature feature : features ) {					
					matrix.getData()[row + cont * matrix.numRows()] = node.getAttributeValue(feature.getFeatureId());
					//matrix.set(row, cont, node.getAttributeValue(feature.getFeatureId()));
					cont++;
				}
			}
			return;
		}
		
		// put difference nodes repeating first node
		for(int i = 0 ; i <= delta-descendantsPaths[node.getId()].size() ; i++ ) {
			for( Feature feature : features ) {						
				matrix.getData()[row + cont * matrix.numRows()] = descendantsPaths[node.getId()].get(0).getAttributeValue(feature.getFeatureId());
				//matrix.set(row, cont, descendantsPaths[node.getId()].get(0).getAttributeValue(feature.getFeatureId()));
				cont++;
			}
		}
		
		// get descendants
		for(int i = 0 ; i < descendantsPaths[node.getId()].size()-1 ; i++ ) {
			NodeCT n = descendantsPaths[node.getId()].get(i);
			for( Feature feature : features ) {
				matrix.getData()[row + cont * matrix.numRows()] = n.getAttributeValue(feature.getFeatureId());
				//matrix.set(row, cont, n.getAttributeValue(feature.getFeatureId()));
				cont++;
			}
		}
	
	}
	
	public Feature[] getFeatures() {		
		return features;		
	}

	public void setFeatures(Feature[] features) {
		this.features = features;
	}

	public ConnectedFilteringByComponentTree getTree() {
		return tree;
	}

	public void setTree(ConnectedFilteringByComponentTree tree) {
		this.tree = tree;
	}

	public Target getTarget() {
		return target;
	}

	public void setTarget(Target target) {
		this.target = target;
	}	
	
	public int getAccumulatedAttributeInPath() {
		return accumulatedAttributeInPath;
	}

	public void setAccumulatedAttributeInPath(int accumulatedAttributeInPath) {
		this.accumulatedAttributeInPath = accumulatedAttributeInPath;
	}
	
}
