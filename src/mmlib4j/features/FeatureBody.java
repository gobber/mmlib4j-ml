package mmlib4j.features;

import mmlib4j.images.ColorImage;

public class FeatureBody {
	
	public String featureName;
	
	public int featureId;
	
	public ColorImage imgRGB;

	public String getFeatureName() {
		
		return featureName;
		
	}
	
	public int getFeatureId() {
		
		return featureId;
	}
	

	public ColorImage getImgRGB() {
		
		return imgRGB;
		
	}


	public void setImgRGB(ColorImage imgRGB) {
		
		this.imgRGB = imgRGB;
		
	}	
	
}
