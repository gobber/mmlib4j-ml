.. mmlib4j-ml documentation master file, created by
   sphinx-quickstart on Mon Jan 28 23:08:24 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mmlib4j-ml's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

:java:type:`Matrix`

